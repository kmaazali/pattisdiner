package com.example.lenovom71e.menuapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.model.Saleable;
import com.android.tonyvu.sc.util.CartHelper;
import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.math.BigDecimal.ROUND_CEILING;

/**
 * Created by aliba on 6/20/2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<Saleable> _listDataHeader ;
    private List<Integer> _listDataHeaderId ;// header titles
    //child data in format of headertitle, child title;
    private ArrayList<Saleable> _listDataChild;
    private HashMap<Integer, List<ExtraProuct>> _listDataChildId;
//    private HashMap<Integer,HashMap<Saleable,List<ExtraProuct>>> _ListDataChildWithId;
//    private  List<String> _listDataChild;
    private HashMap<HashMap<Saleable,List<ExtraProuct>>, Integer> _listHeaderWQuantity;
    private List<Integer> _productQuantities;
    private double _extraPrice;
    final CheckOut checkOut= CheckoutHelper.getCheckout();

//    public ExpandableListAdapter(Context context,
//                                 List<Saleable> listDataHeader,
//                                 HashMap<Saleable, List<ExtraProuct>> listChildData,
//                                 HashMap<HashMap<Saleable,List<ExtraProuct>>, Integer> listHeaderWQuantity,
//                                 List<Integer> productQuantities){
//        this._context = context;
//        this._listDataHeader = listDataHeader;
//        this._listDataChild = listChildData;
//        this._listHeaderWQuantity = listHeaderWQuantity;
//        this._productQuantities = productQuantities;
//
//    }

    public ExpandableListAdapter(Context context,
                                 List<Integer> listDataHeader,
                                 HashMap<Integer, List<ExtraProuct>> listChildData,
                                 HashMap<HashMap<Saleable,List<ExtraProuct>>, Integer> listHeaderWQuantity,
                                 List<Integer> productQuantities,
                                 ArrayList<Saleable> prodcutsWithExtraList){
        this._context = context;
        this._listDataHeaderId = listDataHeader;
        this._listDataChildId = listChildData;
        this._listHeaderWQuantity = listHeaderWQuantity;
        this._productQuantities = productQuantities;
        this._listDataChild = prodcutsWithExtraList;
        checkOut.setIdProductExtraList(listChildData);
        checkOut.setProductHeaderId((ArrayList<Integer>) listDataHeader);

    }

    public interface OnDataChangeListener{
        public void onDataChanged(int size);
    }

    OnDataChangeListener mOnDataChangeListener;
    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener){
        mOnDataChangeListener = onDataChangeListener;
    }

    @Override
    public Object getChild(int groupPosition, int childPositition) {
//        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPositition);
        return this._listDataChildId.get(this._listDataHeaderId.get(groupPosition)).get(childPositition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


//    public double calculateExtraPrice(int groupPosition){
//        double result = 0;
//        int count = getChildrenCount(groupPosition);
//        for (int i=0; i< count ; i++){
//            Extra current = (Extra) getChild(groupPosition, i);
//            result += current.extraPrice;
//
//        }
//        return result;
//    }

    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent){
        ExtraProuct currentString = (ExtraProuct) getChild(groupPosition,childPosition);


//        Log.d("CurrentExtraList : ", String.valueOf(currentExtraList));
        //Log.d("Current String", currentString);
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_extra_item, null);
        }

       // Log.d("GetchildView", expandedListText);

        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        txtListChild.setText(currentString.getExtraName());

        TextView lblChildPrice = (TextView) convertView.findViewById(R.id.lbllistItemPrice);
        lblChildPrice.setText("" + currentString.getExtraPrice());
        return convertView;
    }

    @Override
    public int getChildrenCount (int groupPosition){
//        Saleable currentGroup = _listDataHeader.get(groupPosition);
//        return this._listDataChild.get(currentGroup).size();
        Integer currentGroup = _listDataHeaderId.get(groupPosition);
        return this._listDataChildId.get(currentGroup).size();
    }


    @Override
    public Object getGroup(int groupPosition) {
//        return this._listDataHeader.get(groupPosition);
        return this._listDataHeaderId.get(groupPosition);
    }

    @Override
    public int getGroupCount(){
//        return this._listDataHeader.size();
        return this._listDataHeaderId.size();
    }
    @Override
    public long getGroupId(int groupPosition){
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String currnetName = "";
        BigDecimal price = BigDecimal.ZERO;
        _extraPrice = 0;
        Integer headerId = _listDataHeaderId.get(groupPosition);
        for (Saleable id:_listDataChild){

            if (headerId == id.getitemId()){
                Log.d("group name", id.getPname());
                currnetName = id.getPname();
                price = id.getPrice();
                break;
            }
        }
//        String headerTitle = _listDataHeader.get(groupPosition).getPname();
        String headerTitle = currnetName;
        List<ExtraProuct> currentExtraList = _listDataChildId.get(_listDataHeaderId.get(groupPosition));
        final Integer currentQuantity = _productQuantities.get(groupPosition);
        Log.d("Currnet Quantity : ", String.valueOf(currentQuantity));
        for (int i = 0 ; i < currentExtraList.size(); i++){

            ExtraProuct currentCHild = currentExtraList.get(i);
            _extraPrice += currentCHild.getExtraPrice();
        }
//Log.d("Header title " , headerTitle);
//        int quantityTitle = _listDataHeader.get(groupPosition).getQuantity();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_cart_item, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.tvCartItemName);
        lblListHeader.setText(headerTitle);

        TextView lblProductQuantity = (TextView) convertView.findViewById(R.id.tvCartItemQuantity);
        lblProductQuantity.setText("" + currentQuantity);

//        BigDecimal productPrice = _listDataHeader.get(groupPosition).getProduct().getPrice().multiply(BigDecimal.valueOf(quantityTitle));
//        BigDecimal productPrice = _listDataHeader.get(groupPosition).getPrice();
        BigDecimal productPrice = price;

        //double extraPrice = (calculateExtraPrice(groupPosition) * quantityTitle);
        BigDecimal totalPrice = productPrice.add(BigDecimal.valueOf(_extraPrice)).multiply(BigDecimal.valueOf(currentQuantity));
//        Log.d("Prodcut Price ", String.valueOf(productPrice));
//        Log.d("Extra Price ", String.valueOf(extraPrice));

        TextView lblProductPrice = (TextView) convertView.findViewById(R.id.tvCartItemPrice);
        lblProductPrice.setText("" + productPrice.setScale(2,ROUND_CEILING));
        TextView lblTotalItemPrice = (TextView) convertView.findViewById(R.id.itemTotal);
        lblTotalItemPrice.setText("" + totalPrice);


        final ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);

        //Implement Delete Button in group list
        Button delete = (Button) convertView.findViewById(R.id.del);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Cart cart = CartHelper.getCart();




                //TODO: we need to add remove function for the cart and run the function here
               // cart.remove(groupPosition,_listDataChild,_listHeaderWQuantity); // remove from the cart items

                /*int number = checkOut.getProductHeaderId().get(groupPosition);
                List<ExtraProuct> numberList = checkOut.getIdProductExtraList().get(number);
*/

//                checkOut.getProductHeaderId().remove(groupPosition);

                _listDataChildId.remove(_listDataHeaderId.get(groupPosition));
                _listHeaderWQuantity.remove(_listDataHeaderId.get(groupPosition));
                _listDataHeaderId.remove(_listDataHeaderId.get(groupPosition));
//                _listDataChild.remove(_listDataHeaderId.get(groupPosition));
                _productQuantities.remove(groupPosition);
                cart.getProductsHeader().remove(groupPosition);

//                checkOut.getProductHeaderId().remove(groupPosition);


//                List<ExtraProuct> currentList = checkOut.getIdProductExtraList().get(groupPosition);
//                checkOut.getIdProductExtraList().get(checkOut.getProductHeaderId().get(groupPosition));
//                checkOut.getProductHeaderId().remove(groupPosition);




                ExpandableListAdapter.this.notifyDataSetChanged();
                if(mOnDataChangeListener != null){
//                    mOnDataChangeListener.onDataChanged(_listDataChild.size());
                    mOnDataChangeListener.onDataChanged(_listDataChildId.size());

                }

                notifyDataSetChanged();


            }
        });

        return convertView;
    }

    @Override
    public boolean hasStableIds(){
        return false;
    }
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition){
        return true;
    }
}