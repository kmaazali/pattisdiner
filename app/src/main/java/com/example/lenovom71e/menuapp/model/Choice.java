package com.example.lenovom71e.menuapp.model;

import java.math.BigDecimal;

/**
 * Created by STUDENT on 2/24/2016.
 */

public class Choice {



    private String cName;
    private BigDecimal cPrice;
    private int multiple;
    private int cbValue;
    private int idE;
    private String eName;


    public Choice(){
    }

    public Choice(int id, String name){
        cName = name;
        idE = id;
    }

    public String getCname() {
        return cName;
    }

    public String setCname(String cName) {
        this.cName = cName;
        return cName;
    }
    public BigDecimal getCprice() {
        return cPrice;
    }

    public BigDecimal setCprice(BigDecimal cPrice) {
        this.cPrice = cPrice;
        return cPrice;
    }
    public int getCmultiple() {
        return multiple;
    }

    public int setCmultiple(int multiple) {
        this.multiple = multiple;
        return multiple;
    }
    public int getcbValue(){
        return this.cbValue;
    }
    public int getIde() {
        return idE;
    }

    public int setIde(int idE) {
        this.idE = idE;
        return idE;
    }

    public String getEname() {
        return eName;
    }

    public String setEname(String eName) {
        this.eName = eName;
        return eName;
    }
}

