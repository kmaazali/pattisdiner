package com.example.lenovom71e.menuapp;

/**
 * Created by LENOVO on 5/9/2016.
 */

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.model.Saleable;
import com.android.tonyvu.sc.util.CartHelper;
import com.example.lenovom71e.menuapp.adapter.ExpandableListAdapter;
import com.example.lenovom71e.menuapp.model.CartItem;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Product;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.lang.System.currentTimeMillis;

public class ShoppingCartActivity extends AppCompatActivity {
    private static final String TAG = "ShoppingCartActivity";

    ListView lvCartItems;
    Button bClear;
    Button bShop;
    TextView tvTotalPrice;
    TextView totalPriceLabel;
    TextView taxValueLabel;

    //Declair Variables BY Ali Bahrani
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<CartItem> listDataHeader = new ArrayList<>();
    HashMap<Integer, List<ExtraProuct>> listDataChild = new HashMap<>();
    ArrayList<String> extraName = new ArrayList<String>();
    List<String> extraNameList = new ArrayList<String>();
    ArrayList<Integer> headers = new ArrayList<>();
    HashMap<Integer, List<ExtraProuct>> extraList = new HashMap<>();
    BigDecimal result = BigDecimal.ZERO;
    ArrayList<Saleable> headerProducts = new ArrayList<>();
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");


    private final Handler handler = new Handler();

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_cart);

//        lvCartItems = (ListView) findViewById(R.id.lvCartItems);
//        LayoutInflater layoutInflater = getLayoutInflater();








        final Cart cart = CartHelper.getCart();

        //Adding intent to the activity By Ali Bahrani
        final Intent intent = this.getIntent();
        extraName = (ArrayList<String>) getIntent().getSerializableExtra("extra2List");



        headers =  cart.getProductHeaderId(); // header items
        Log.d("Headers List from Detail: ", String.valueOf(headers));
        extraList = cart.getIdProductExtraList(); // child elements
        Log.d("HasMap from Detail: ", String.valueOf(extraList));
        extraNameList = extraName;


        //Define ExpandableList By Ali Bahrani
        //get the listView
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        //preparing list data
//        listDataHeader = getCartItems(cart);
//        listDataHeader = getCartItems(cart);
        final HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> listDataHeaderWQuantity = cart.getProductExtraListMap();
        final List<Integer> productQuantities = cart.getProductQuantities();
        final HashMap<Saleable, List<ExtraProuct>> productsWithExtraList = cart.getProductExtraList();
        headerProducts = (ArrayList<Saleable>) cart.getProductsHeader();


        Log.d("ListDataHeader : " , String.valueOf(listDataHeader));
        listDataChild = extraList;
//        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        //listAdapter = new ExpandableListAdapter(this, headers, extraList, listDataHeaderWQuantity, productQuantities);
        listAdapter = new ExpandableListAdapter(this, headers, extraList, listDataHeaderWQuantity, productQuantities, headerProducts);

        expListView.setAdapter(listAdapter);


        for (int i= 0; i < headers.size(); i ++) {
            result = result.add(cart.getProductTotalPrice(headers,extraList,i,productQuantities.get(i)));
        }


        Log.d("Result of the productItem is : ", String.valueOf(result));

        final TextView tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
        //tvTotalPrice.setText(String.valueOf(cart.getTotalPrice()) );
        tvTotalPrice.setText("" + result);


        totalPriceLabel = (TextView) findViewById(R.id.tvGrandTotal);

        totalPriceLabel.setText(String.valueOf(cart.getGrandTotalPrice(result,cart.getTaxValue(result))));
        taxValueLabel = (TextView) findViewById(R.id.tvTax);
        taxValueLabel.setText(String.valueOf(cart.getTaxValue(result)));



        //Update View when item deleted from the list

        listAdapter.setOnDataChangeListener(new ExpandableListAdapter.OnDataChangeListener() {
            public void onDataChanged(int size){
                //do whatever here
                BigDecimal result = BigDecimal.ZERO;

//                headers =  cart.getProductsHeader(); // header items
                headers = cart.getProductHeaderId();
//                extraList = cart.getExtraProduct(); // child elements
                extraList = cart.getIdProductExtraList();
                List<Integer> productQuantities = cart.getProductQuantities(); // headers Quantities
                for (int i= 0; i < headers.size(); i ++) {
                    result = result.add(cart.getProductTotalPrice(headers,extraList,i,productQuantities.get(i)));
                }

                tvTotalPrice.setText(String.valueOf(result));
                taxValueLabel.setText(String.valueOf(cart.getTaxValue(result)));
                totalPriceLabel.setText(String.valueOf(cart.getGrandTotalPrice(result,cart.getTaxValue(result))));


            }
        });



        //lvCartItems.addHeaderView(layoutInflater.inflate(R.layout.cart_header, lvCartItems, false));

//        final CartItemAdapter cartItemAdapter = new CartItemAdapter(this);
//        cartItemAdapter.updateCartItems(getCartItems(cart));
//
//
//        Log.d("passed extra: ", String.valueOf(extraName));

        //lvCartItems.setAdapter(cartItemAdapter);

        bClear = (Button) findViewById(R.id.bCheckout);
        bShop = (Button) findViewById(R.id.bShop);

        bClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Clearing the shopping cart");
                HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> listDataHeaderWQuantities = cart.getProductExtraListMap();
                //TODO: Implement the array of (headers, extraList, listDataHeaderWQuantity, productQuantities, headerProducts)
                int productId, productQnatity, extraId;

                final HashMap<HashMap<Saleable, List<ExtraProuct>>, Integer> listDataHeaderWQuantity = cart.getProductExtraListMap();
                final List<Integer> productQuantities = cart.getProductQuantities();
                final HashMap<Saleable, List<ExtraProuct>> productsWithExtraList = cart.getProductExtraList();
                headerProducts = (ArrayList<Saleable>) cart.getProductsHeader();

                CheckOut checkout = CheckoutHelper.getCheckout();
                ArrayList<Integer> listHeader = checkout.getProductHeaderId();
                String tax = (String) taxValueLabel.getText();
                checkout.setmTaxValue(BigDecimal.valueOf(Double.valueOf(tax)));
                checkout.setmSubTotal(BigDecimal.valueOf(Double.valueOf(Double.valueOf((String) tvTotalPrice.getText()))));
                checkout.setmCheckOutTotal(BigDecimal.valueOf(Double.valueOf((String) totalPriceLabel.getText())));
                checkout.setmQuantity(headers.size());
                checkout.setProductQuantities((ArrayList<Integer>) productQuantities);
                ArrayList<ExtraProuct> extras = new ArrayList<>();

                int pid = 0;
                BigDecimal pPrice = BigDecimal.ZERO;

                for (int i = 0; i < headerProducts.size(); i++){


                    int headerid = headerProducts.get(i).getitemId();
                    productId = headers.get(i);
                    if (headerid == productId){
                        Saleable currentProduct  = headerProducts.get(i);
                        pid = currentProduct.getproductId();
                        pPrice = currentProduct.getPrice();
                        extras.addAll(extraList.get(productId));
                        Log.d("Items are : ", "Product id" + pid + " price: " + pPrice + " Extras :" + extras.get(i).getExtraId());
                    }
                    Log.d("product id is : " , String.valueOf(pid));
                    headerProducts.get(i).getPrice();
                    productQnatity = productQuantities.get(i);
                    extraList.get(productId);
                    Log.d("checkout variables =  ", "" + productId + " " + productQnatity + " " + extraList.get(productId));
                }

                //Make Post using Volley Library

                String CurrentTime = getTimeStamp();
                cart.clear();
                getTimeStamp();
//                cartItemAdapter.updateCartItems(getCartItems(cart));
//                cartItemAdapter.notifyDataSetChanged();
                tvTotalPrice.setText(String.valueOf(cart.getTotalPrice()));
                taxValueLabel.setText("0.00");
                totalPriceLabel.setText("0.00");

                Intent intent2 =  new Intent(ShoppingCartActivity.this, CheckoutActivity.class);
                startActivity(intent2);

            }


        });

        bShop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShoppingCartActivity.this, CategoriesActivity.class);
                startActivity(intent);
            }
        });


    }



    private String getTimeStamp(){
        Long tsLong  = currentTimeMillis();
        return sdf.format(tsLong);

    }

    private List<CartItem> getCartItems(Cart cart) {
        List<CartItem> cartItems = new ArrayList<CartItem>();
        Log.d(TAG, "Current shopping cart: " + cart);

        Map<Saleable, Integer> itemMap = (Map<Saleable, Integer>) cart.getItemWithQuantity();

        for (Entry<Saleable, Integer> entry : itemMap.entrySet()) {
            CartItem cartItem = new CartItem();
            cartItem.setProduct((Product) entry.getKey());
            cartItem.setQuantity(entry.getValue());
            cartItems.add(cartItem);
        }

        Log.d(TAG, "Cart item list: " + cartItems);
        return cartItems;
    }

}
