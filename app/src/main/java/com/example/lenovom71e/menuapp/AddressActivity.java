package com.example.lenovom71e.menuapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

public class AddressActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        final EditText sFirstName = (EditText) findViewById(R.id.sFirstName);
        final EditText sLastName = (EditText) findViewById(R.id.sLastname);
        final EditText newAddress = (EditText) findViewById(R.id.newAddress);
        final EditText newAddress1 = (EditText) findViewById(R.id.newAddress1);
        final EditText newCity = (EditText) findViewById(R.id.newCity);
        final EditText newProvince = (EditText) findViewById(R.id.newProvince);
        final EditText newPostcode = (EditText) findViewById(R.id.newPostCode);

        final EditText deliveryCompanyName = (EditText) findViewById(R.id.shippingCompanyNameNew);
        final EditText deliveryCompanyEmail = (EditText) findViewById(R.id.shippingCompanyEmail);
        final EditText shippingFirstname = (EditText) findViewById(R.id.sFirstName);
        final  EditText shippingLastname = (EditText) findViewById(R.id.sLastname);

        Button submitNewAddress = (Button) findViewById(R.id.bSubmitNewAddress);

        final CheckOut checkout = CheckoutHelper.getCheckout();


        submitNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkout.setDeliveryAddress(newAddress.getText().toString());
                checkout.setDeliveryAddress(newAddress1.getText().toString());
                checkout.setDeliveryCity(newCity.getText().toString());
                checkout.setDeliveryProvince(newProvince.getText().toString());
                checkout.setDeliveryPostcode(newPostcode.getText().toString());
                checkout.setDeliveryCompanyEmail(deliveryCompanyEmail.getText().toString());
                checkout.setDeliveryCompanyName(deliveryCompanyName.getText().toString());
                checkout.setShippingFirstName(sFirstName.getText().toString());
                checkout.setShippingLastName(sLastName.getText().toString());
                //Adding shipping firstname and lastname by Ali March 2nd,2018
                checkout.getShippingFirstName(shippingFirstname.getText().toString());
                checkout.getShippingLastName(shippingLastname.getText().toString());


                finish();
            }
        });
    }
}
