package com.example.lenovom71e.menuapp.model;

/**
 * Created by admin on 2017-08-14.
 */

public class UserAccount {

    private  String first_name,last_name,phone,email,password,company,address1,address2,city,province,postcode,country;

    private String user_id;

    public UserAccount(String first_name,String last_name,String phone,String email,String password,
                       String company,String address1,String address2,String city,String province,String postcode,String country){

        this.first_name = first_name;
        this.last_name = last_name;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.company = company;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.province = province;
        this.postcode = postcode;
        this.country = country;

    }
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

}
