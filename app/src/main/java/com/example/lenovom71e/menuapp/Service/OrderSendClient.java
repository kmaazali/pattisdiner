package com.example.lenovom71e.menuapp.Service;

import com.example.lenovom71e.menuapp.model.Paymentconfirm;
import com.example.lenovom71e.menuapp.model.PostProduct;
import com.example.lenovom71e.menuapp.util.OrderSend;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by admin on 2/5/2018.
 */

public interface OrderSendClient {

    @Multipart
    @POST("placeOrder/")
    Call<PostProduct> sendOrder2(@Part("date") String date,
                                 @Part("delivery_charge") String delivery_charge,
                                 @Part("from") Integer from,
                                 @Part("items[]") List<OrderSend.Item> items,
                                 @Part("note") String note,
                                 @Part("time") String time,
                                 @Part("type") String type,
                                 @Part("coupon") String coupon,
                                 @Part("userid") Integer userid,
                                 @Part("bfname") String bfname,
                                 @Part("blname") String blname,
                                 @Part("baddress") String baddress,
                                 @Part("baddress1") String baddress1,
                                 @Part("bcity") String bcity,
                                 @Part("bstate") String bstate,
                                 @Part("bpostcode") String bpostcode,
                                 @Part("bphone") String bphone,
                                 @Part("bcompany") String bcompany,
                                 @Part("bemail") String email,
                                 @Part("shipping_first_name") String sFirstname,
                                 @Part("shipping_last_name") String sLastname,
                                 @Part("shipping_address_1") String shippingAddress,
                                 @Part("shipping_address_2") String shippingAddress2,
                                 @Part("shipping_city") String dCity,
                                 @Part("shipping_state") String dState,
                                 @Part("shipping_postcode") String dPostcode,
                                 @Part("shipping_company") String dCompany,
                                 @Part("shipping_email") String demail
                                 );

    @Multipart
    @POST("payment/")
    Call<Paymentconfirm> makePayment(@Part("id") Integer id,
                                     @Part("total") Double total,
                                     @Part("full_name")String fullname,
                                     @Part("cc_number")Long cardNumber,
                                     @Part("expiry") String expiryDate,
                                     @Part("cvc") Integer cvc
                                     );



//    ("id")("total")("full_name")("cc_number")("expiry") ("cvc")
}
