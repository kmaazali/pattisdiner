package com.example.lenovom71e.menuapp;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.adapter.CustomTimeAdapter;
import com.example.lenovom71e.menuapp.model.Address;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by admin on 2017-07-19.
 */

public class CheckoutActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Button date;
    Spinner time;
    DatePickerDialog datePickerDialog;
    final Calendar c = Calendar.getInstance();
    Spinner spin ;
    //Parsing Data from Url
    String JsonURL = "https://demo.online-menu.ca/api/location" ;
    String data = "";
    RequestQueue requestQueue;
    ArrayList<Address> addresses = new ArrayList<>();
    String openTime, closeTime;
    Cart cart = CartHelper.getCart();
    int dateIntger;


    RadioGroup rg;
    private Boolean mIgnoreEvent = false;
    private Boolean deliveryIsChanged = false;

    String[] timeArray = {"7:00 AM", "7:30 AM", "8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM",
            "12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM"
            , "5:00 PM", "5:30 PM", "6:00 PM", "6:30 PM", "7:00 PM", "7:30 PM", "8:00 PM", "8:30 PM", "9:00 PM", "9:30 PM"
            , "10:00 PM", "10:30 PM", "11:00 PM", "11:30 PM", "12:00 AM"};
    ArrayList<String> currentTimeArray = new ArrayList<>();





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_activity);
        final Button locationbtn = (Button) findViewById(R.id.locationButton);
        final View v = findViewById(R.id.locationGroup);
        final View v1 = findViewById(R.id.addressLabel);
        final View dateTimeGroupView = findViewById(R.id.dateTimeGroup);
        final CheckOut checkOut = CheckoutHelper.getCheckout();
        final TextView nameLbl = (TextView) findViewById(R.id.nameLbl);
        final TextView addrresLbl= (TextView) findViewById(R.id.addressLbl);
        final TextView cityLbl= (TextView) findViewById(R.id.cityLbl);
        final TextView phoneLbl= (TextView) findViewById(R.id.phoneLbl);
        final Button continueBtn = (Button) findViewById(R.id.continueCheckoutBtn);
        dateTimeGroupView.setVisibility(View.INVISIBLE);
        final TextView hoursWork = (TextView) findViewById(R.id.hoursLabel);
        final Button cancelButton = (Button) findViewById(R.id.cancelCheckoutBtn);
        final EditText checkoutInstruction = (EditText) findViewById(R.id.checkoutInstruction);


        continueBtn.setClickable(false);



        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray objectArray = response.getJSONArray("locations");
                    int count = 0;
                    for(int i = 0; i <objectArray.length(); i++){
                        JSONObject currentAddress = objectArray.getJSONObject(i);
                        Address address = new Address();
                        address.setmAddress(currentAddress.getString("address") + ", " + currentAddress.getString("postal_code"));
                        address.setmName(currentAddress.getString("name"));
                        address.setmCity(currentAddress.getString("city"));
                        address.setmPhoneNumber(currentAddress.getString("phone"));
                        address.setId(Integer.parseInt(currentAddress.getString("idlocation")));
                        addresses.add(address);
//

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "Error");
            }
        });
        requestQueue.add(objectRequest);





        locationbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent locationIntent = new Intent(CheckoutActivity.this, LocationActivity.class);
                locationIntent.putExtra("extraLocations", addresses);

                startActivity(locationIntent);

            }
        });


        //Display Work hours




        dateTimeGroupView.setVisibility(View.GONE);

        RadioButton rDelivery = (RadioButton) findViewById(R.id.delivery);
        RadioButton rEaton = (RadioButton) findViewById(R.id.eatin);
        final RadioButton sameAddress = (RadioButton) findViewById(R.id.sameAddress);
        RadioButton differentAddress =  (RadioButton) findViewById(R.id.differentAdrress);
        final RadioGroup locationGroup  = (RadioGroup) findViewById(R.id.deliveryChange);
        final TextView changeAddressLbl  = (TextView) findViewById(R.id.deliveryLocation);
        rg = (RadioGroup) findViewById(R.id.orderType);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){
                    case R.id.delivery:
                        checkOut.setDeliver(true);
                        checkOut.setEatin(false);
                        checkOut.setPickup(false);
                        continueBtn.setEnabled(false);
                        v.setVisibility(View.GONE);
                        v1.setVisibility(View.GONE);
                        changeAddressLbl.setVisibility(View.VISIBLE);
                        locationGroup.setVisibility(View.VISIBLE);
                        locationGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId1) {
                                switch (checkedId1) {
                                    case R.id.sameAddress:
                                        deliveryIsChanged = false;
                                        checkOut.setDeliveryIsDifferent(deliveryIsChanged);
                                        break;
                                    case R.id.differentAdrress:
                                        Intent intent = new Intent(CheckoutActivity.this, AddressActivity.class);
                                        startActivity(intent);
                                        deliveryIsChanged = true;
                                        checkOut.setDeliveryIsDifferent(deliveryIsChanged);
                                }
                            }
                        });
                        Log.d("Radio Checkout", "Delivery is selected ");
                        checkOut.setBtnorderTypeId(R.id.delivery);
                        dateTimeGroupView.setVisibility(View.VISIBLE);
                        //TODO: Set the Delivery id = 1
                        checkOut.setOrderTypeId(1);
                        checkOut.setProviderAddress(addresses.get(0));

                        //make open now method
                        requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
                        JsonObjectRequest objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {


                                    TextView hoursWork = (TextView) findViewById(R.id.hoursLabel);
                                    JSONObject objectArray = response.getJSONObject("hours");
                                    Log.d("Json Object Menthod", String.valueOf(objectArray));
                                    JSONObject open = objectArray.getJSONObject("delivery");
                                    openTime = open.getString("open");
                                    closeTime = open.getString("close");
//                                    checkOut.getTimeperiod(openTime,closeTime);
                                    //Load timeSpinner data
                                    String[] stringarrray = new String[currentTimeArray.size()];
                                    stringarrray = checkOut.getTimeperiod(openTime, closeTime).toArray(stringarrray);
//                                  //TODO:Setup Time Spinner
                                    CustomTimeAdapter customTimeAdapter = new CustomTimeAdapter(getApplicationContext(), stringarrray);
                                    customTimeAdapter.notifyDataSetChanged();
                                    time.setVisibility(View.INVISIBLE);
                                    time.setAdapter(customTimeAdapter);
//

                                    if(open.getString("closed")== "1") {
                                        hoursWork.setText("We are closed now");
                                    }else {
                                        hoursWork.setText("Delivery open from: " + open.getString("open") + " To " + open.getString("close"));

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("Volley", "Error");
                            }
                        });

                        requestQueue.add(objectRequest);


                        break;
                    case R.id.eatin:
                        checkOut.setDeliver(false);
                        checkOut.setEatin(true);
                        checkOut.setPickup(false);
                        continueBtn.setEnabled(false);
                        v.setVisibility(View.VISIBLE);
                        dateTimeGroupView.setEnabled(false);
                        dateTimeGroupView.setVisibility(View.GONE);
                        String currentName = getIntent().getStringExtra("companyName");
                        if (currentName != null ){
                            v1.setVisibility(View.VISIBLE);
                            nameLbl.setText(getIntent().getStringExtra("companyName"));
                            addrresLbl.setText(getIntent().getStringExtra("companyAddress"));
                            cityLbl.setText(getIntent().getStringExtra("companyCity"));
                            phoneLbl.setText(getIntent().getStringExtra("companyPhone"));


                        }
                        checkOut.setBtnorderTypeId(R.id.eatin);
                        if(checkOut.getProviderAddress() == null){
                            dateTimeGroupView.setVisibility(View.INVISIBLE);

                        }
                        //TODO: Set the Eatin id = 1 and address
                        checkOut.setOrderTypeId(2);
                        if (checkOut.getProviderAddress() != null){
                            dateTimeGroupView.setVisibility(View.VISIBLE);
                            //make open now method
                            requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
                            objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    try {

                                        TextView hoursWork = (TextView) findViewById(R.id.hoursLabel);
                                        JSONObject objectArray = response.getJSONObject("hours");
                                        Log.d("Json Object Menthod", String.valueOf(objectArray));
                                        JSONObject open = objectArray.getJSONObject("working");
                                        openTime = open.getString("open");
                                        closeTime = open.getString("close");
                                        //Load timeSpinner data
                                        String[] stringarrray = new String[currentTimeArray.size()];
                                        stringarrray = checkOut.getTimeperiod(openTime, closeTime).toArray(stringarrray);
//                                      //TODO:Setup Time Spinner
                                        CustomTimeAdapter customTimeAdapter = new CustomTimeAdapter(getApplicationContext(), stringarrray);
                                        customTimeAdapter.notifyDataSetChanged();
                                        time.setAdapter(customTimeAdapter);

                                        if(open.getString("closed")== "1") {
                                            hoursWork.setText("We are closed now");
                                        }else {
                                            hoursWork.setText("We are open from: " + open.getString("open") + " To " + open.getString("close"));
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("Volley", "Error");
                                }
                            });

                            requestQueue.add(objectRequest);
                        }


                        break;
                    case R.id.pickup:
                        checkOut.setDeliver(false);
                        checkOut.setEatin(false);
                        checkOut.setPickup(true);
                        v.setVisibility(View.INVISIBLE);
                        hoursWork.setVisibility(View.GONE);
                        continueBtn.setEnabled(false);
                        v.setVisibility(View.VISIBLE);
                        dateTimeGroupView.setEnabled(false);
                        dateTimeGroupView.setVisibility(View.GONE);
                         currentName = getIntent().getStringExtra("companyName");
                        if (currentName != null ){
                            v1.setVisibility(View.VISIBLE);
                            nameLbl.setText(getIntent().getStringExtra("companyName"));
                            addrresLbl.setText(getIntent().getStringExtra("companyAddress"));
                            cityLbl.setText(getIntent().getStringExtra("companyCity"));
                            phoneLbl.setText(getIntent().getStringExtra("companyPhone"));


                        }
                        checkOut.setBtnorderTypeId(R.id.eatin);
                        if(checkOut.getProviderAddress() == null){
                            dateTimeGroupView.setVisibility(View.INVISIBLE);

                        }
                        //TODO: Set the Pickup id = 1
                        checkOut.setOrderTypeId(3);
                        if (checkOut.getProviderAddress() != null){
                            dateTimeGroupView.setVisibility(View.VISIBLE);
                            //make open now method
                            requestQueue = Volley.newRequestQueue(CheckoutActivity.this);
                            objectRequest = new JsonObjectRequest(JsonURL, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    try {
                                        JSONObject objectArray = response.getJSONObject("hours");
                                        Log.d("Json Object Menthod", String.valueOf(objectArray));
                                        JSONObject open = objectArray.getJSONObject("working");
                                        //Load timeSpinner data
                                        String[] stringarrray = new String[currentTimeArray.size()];
                                        stringarrray = checkOut.getTimeperiod(openTime, closeTime).toArray(stringarrray);
//                                      //TODO:Setup Time Spinner
                                        CustomTimeAdapter customTimeAdapter = new CustomTimeAdapter(getApplicationContext(), stringarrray);
                                        customTimeAdapter.notifyDataSetChanged();
                                        time.setAdapter(customTimeAdapter);

                                        if(open.getString("closed")== "1") {
                                            hoursWork.setVisibility(View.VISIBLE);
                                            hoursWork.setText("We are closed now");
                                        }else {
                                            hoursWork.setVisibility(View.VISIBLE);
                                            hoursWork.setText("We are open from: " + open.getString("open") + " To " + open.getString("close"));
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("Volley", "Error");
                                }
                            });

                            requestQueue.add(objectRequest);
                        }
                        break;

                }
            }
        });

        //initiate the date picker and the button and time
        date = (Button) findViewById(R.id.dateBtn);
        time = (Spinner) findViewById(R.id.timeBtn);
        time.setOnItemSelectedListener(this);



        if (checkOut.getBtnorderTypeId()> 0 ) {
            RadioButton rdbtn = (RadioButton) findViewById(checkOut.getBtnorderTypeId());
            rdbtn.setChecked(true);
            checkOut.setBtnorderTypeId(0);
        }

        // Perform click event on button

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calender class instance and current date



                final int mYear  = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                final int mDay = c.get(Calendar.DAY_OF_MONTH);

                //retieve current date
                final java.util.Date current = Calendar.getInstance().getTime();



                // Date picker dialog
                datePickerDialog= new DatePickerDialog(CheckoutActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        view.setMinDate(System.currentTimeMillis() - 1000);
                        Calendar selectedCal =  Calendar.getInstance();
                        selectedCal.set(year,monthOfYear,dayOfMonth);

                        java.util.Date selectedDate = selectedCal.getTime();
                        if (selectedDate.getTime() < current.getTime()) {
                            Log.d("checkoutActivity", "this is less time ");
                            Toast.makeText(CheckoutActivity.this,"Please choose a valid date !!",Toast.LENGTH_LONG).show();

                        }else {
                            date.setText(dayOfMonth + "/" + (monthOfYear + 1) +"/" + year);
                            //TODO: Save the date into checkout helper method
                            checkOut.setOrderDate(date.getText().toString());
                            time.setVisibility(View.VISIBLE);

                            Log.d("compare Date", String.valueOf(date));
                        }

                    }
                }, mYear,mMonth,mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 604800000);
                datePickerDialog.show();
            }
        });


        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Save checkout Insturciton
                checkOut.setCheckoutInstruction(checkoutInstruction.getText().toString());
//                Toast.makeText(CheckoutActivity.this, "continue clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(CheckoutActivity.this, LoginActivity.class);
                //Save all Data
                // TODO : Add all checkout Data
//                Log.d("the quanitity is", checkOut.toString());
                Cart cart = CartHelper.getCart();
//                Log.d("Cart is : ", cart.getProductExtraList().toString());
                startActivity(intent);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear Checkout
                checkOut.clearCheckout();
                cart.clear();


                Intent cancelIntent =  new Intent(CheckoutActivity.this, CategoriesActivity.class);
                startActivity(cancelIntent);
            }
        });

    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Button continueBtn = (Button) findViewById(R.id.continueCheckoutBtn);
        continueBtn.setEnabled(false);

        CheckOut checkout = CheckoutHelper.getCheckout();
        String[] stringarrray = new String[currentTimeArray.size()];
        stringarrray = checkout.getTimeperiod(openTime, closeTime).toArray(stringarrray);

        //Check if the time selected or not

        if (stringarrray[position].equals(stringarrray[0])){
            Toast.makeText(getApplicationContext(),"Please select time", Toast.LENGTH_LONG).show();
            continueBtn.setEnabled(false);
        }else {

            String time = stringarrray[position];
            Calendar c = Calendar.getInstance();
            int currentHour = c.get(Calendar.HOUR);
            int currentMinute = c.get(Calendar.MINUTE);
            int AM_FM = c.get(Calendar.AM_PM);
            String[] parts = time.split(":");
            //TODO : Continue to finish the time
            String[]  StringAMPM = parts[1].split(" ");
            int ampmFromList = checkAMPM(StringAMPM[1]);
            final int mYear  = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH) + 1;
            final int mDay = c.get(Calendar.DAY_OF_MONTH);
            String currentDate = mDay + "/" + mMonth  +"/" + mYear;

            if (checkout.getOrderDate() == null && checkout.getOrderDate().isEmpty()) {
                //Todo pick a date
                Toast.makeText(CheckoutActivity.this,"Please Choose date first", Toast.LENGTH_SHORT).show();
            }else {
                dateIntger = checkout.getOrderDate().compareTo(currentDate);

                if (dateIntger > 0){
                    checkout.setOrderTime(stringarrray[position]);
                    continueBtn.setEnabled(true);

                }else if (dateIntger < 0){
                    showAlertDialogTime("its not a valid date please choose a valid date");
                    continueBtn.setEnabled(false);


                }else if (dateIntger == 0){
                    if (checkAMPM(StringAMPM[1]) > AM_FM) {
                        if (Integer.parseInt(parts[0]) > currentHour ){

                            continueBtn.setEnabled(true);
                        }else if (Integer.parseInt(parts[0]) < currentHour){

                            continueBtn.setEnabled(true);
                        }else if (Integer.parseInt(parts[0]) == currentHour){

                            continueBtn.setEnabled(true);
                        }
                        checkout.setOrderTime(stringarrray[position]);
                    }else if (checkAMPM(StringAMPM[1]) == AM_FM){
                        if (Integer.parseInt(parts[0]) > currentHour){
                            if (Integer.parseInt((parts[0])) == 12){
                                continueBtn.setEnabled(false);
                                showAlertDialogTime("please choose future time");
                            }else {
                                checkout.setOrderTime(stringarrray[position]);
                                continueBtn.setEnabled(true);
                                checkout.setOrderTime(stringarrray[position]);
                            }

//                        continueBtn.setEnabled(true);
                        }else if (Integer.parseInt(parts[0]) < currentHour){

                            showAlertDialogTime("please choose future time");
                            continueBtn.setEnabled(false);
                        }else if (Integer.parseInt(parts[0]) == currentHour){
                            checkout.setOrderTime(stringarrray[position]);
                            continueBtn.setEnabled(true);

                        }
                    }else if (checkAMPM(StringAMPM[1]) < AM_FM) {

                        showAlertDialogTime("please choose a valid time or date");
                        continueBtn.setEnabled(false);
                    }
                }
            }




            Toast.makeText(getApplicationContext(), stringarrray[position], Toast.LENGTH_LONG).show();
        }


    }

    public void showAlertDialogTime(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setNegativeButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do nothing
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public String  checkCurrentTime() {
        Calendar c = Calendar.getInstance();
        int currentHour = c.get(Calendar.HOUR);
        int currentMinute= c.get(Calendar.MINUTE);
        int AMPM = c.get(Calendar.AM_PM);
        String timeAMPM = "AM";
        if (AMPM == 1) {
            timeAMPM = "PM";
        }

        return currentHour + ":" + currentMinute + " " + timeAMPM;


    }

    public int checkAMPM(String ampm){
        int timePeriod = 0;
        if (ampm.contentEquals("PM")){
            timePeriod = 1;
        }else if (ampm.contentEquals("AM")){
            timePeriod = 0;
        }
        return timePeriod;
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onBackPressed(){

    }

}
