package com.example.lenovom71e.menuapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.model.Saleable;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.Service.OrderSendClient;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.PostProduct;
import com.example.lenovom71e.menuapp.util.OrderSend;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PassLoginActivity extends AppCompatActivity {

    private String userId, accountNumber, email,firstName,LastName, orderType, postcode;
    private static final String DELIVERY_URL = "https://demo.online-menu.ca/api/delivery/";
    RequestQueue requestQueue;
    String delivery_charge = "0";
    String orderNumber = "";
    String total = "";
    String coupon= "0";
    String dateToString;
    String address = "";
    String address1 = "";
    String city = "";
    String postCode = "";
    String province = "";
    String firstname = "";
    String lastName = "";
    String phoneNumber = "";
    String user = "0";
    String deliveryAddreess="",deliveryAddress2="",deliverypostCode="",deliveryCity="",deliveryProvince="",companyName="",companyEmail="",tipEditText = "0";
    double tipValue = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_login);
        TextView welcomeText = (TextView) findViewById(R.id.welcomeMessage);
        TextView accountNumberText = (TextView) findViewById(R.id.billAccountNumber);
        TextView emailText = (TextView) findViewById(R.id.billEmail);
        TextView phoneText = (TextView) findViewById(R.id.billPhone);
        TextView orderTypeId = (TextView) findViewById(R.id.billOrderId);
        TextView orderDateText = (TextView) findViewById(R.id.billOrderDate);
        final TextView totalPaymentText = (TextView) findViewById(R.id.billTotalPayment);

        final TextView billPriceText = (TextView) findViewById(R.id.billPrice);

        Button placeOrderButton = (Button) findViewById(R.id.placeOrder);
        Button couponDiscountBtn = (Button) findViewById(R.id.discuoutButton);
        final TextView orderTimeText = (TextView) findViewById(R.id.billOrderTime);
        final EditText discountCodeTxt = (EditText) findViewById(R.id.discountCodeTxt);
        TextView notGuest = (TextView) findViewById(R.id.notUserTxt);
        requestQueue = Volley.newRequestQueue(this);
        final TextView deliveryLabel = (TextView) findViewById(R.id.delivery_Charge);
        final EditText tip =  (EditText) findViewById(R.id.tip);



        if (SaveSharedPreference.getAccountAsGuest(getApplicationContext()) == true){
            firstName = SaveSharedPreference.getGuestfname(getApplicationContext());
            LastName = SaveSharedPreference.getGuestlname(getApplicationContext());
            accountNumber = "Guest";
            email = "N/A";
            phoneNumber = SaveSharedPreference.getGuestphone(getApplicationContext());
            notGuest.setText("Not guest ? Please Click here to Sign in / Sign up");
            postcode = SaveSharedPreference.getGuestPostcode(getApplicationContext());
        }else {
            accountNumber = getIntent().getStringExtra("userId");
            firstName = getIntent().getStringExtra("name");
            LastName = getIntent().getStringExtra("lastName");
            email = getIntent().getStringExtra("email");
            phoneNumber = getIntent().getStringExtra("phone");
            postcode = getIntent().getStringExtra("postcode");
            userId = getIntent().getStringExtra("userId");
            notGuest.setText("You are not "+ firstName + " ? Please Click here to sign in / Sign up");
        }





        notGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PassLoginActivity.this, LoginActivity.class);
                SaveSharedPreference.clearUserName(getApplicationContext());
                startActivity(intent);
            }
        });

        //Log.d("LoginPassActivity", accountNumber);
        welcomeText.setText("Thanks for Logging in, "+firstName + " " + LastName);
        accountNumberText.setText(accountNumber);
        emailText.setText(email);
        phoneText.setText(phoneNumber);



        //so lets assume i going to use the checkout to store data from this class
        //all what i have to do is to save to the checkout

        final CheckOut checkOut= CheckoutHelper.getCheckout();
        final Cart cart = CartHelper.getCart();


        checkOut.setmCheckoutTotalAfterDiscount(checkOut.getmCheckOutTotal());
        if (checkOut.isDeliver()){
            orderType = "Delivery";

            if (checkOut.getDeliveryIsDifferent() == true) {
                postcode = checkOut.getDeliveryPostcode();
            }


            deliveryLabel.setText(delivery_charge + "CAD");

            JsonObjectRequest obreq = new JsonObjectRequest(DELIVERY_URL + postcode, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Delivery Json", response.toString());
                    try{
                        Boolean deliveryStatus =  response.getBoolean("delivery");

                        if (deliveryStatus == false && checkOut.getDeliveryIsDifferent() == false){
                            showAlertMessage("Please enter different address");
                            //TODO: retrieve Delivery Charge
                            updateTextViewTotal(delivery_charge,coupon,tipEditText);


                        }else {
                            delivery_charge = response.getString("delivery_charge");
                            // update delivery charge label and added to the total
                            deliveryLabel.setText(delivery_charge + "CAD");
                            updateTextViewTotal(delivery_charge,coupon,tipEditText);
                        }

                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Delivery Json", "Not working");
                }
            });

            requestQueue.add(obreq);


        }else if(checkOut.isEatin()){
            orderType = "Eatin";
            delivery_charge = "0";

        }else if (checkOut.isPickup()){
            orderType = "Pickup";
            delivery_charge = "0";
        }

        orderTypeId.setText(orderType);
        orderDateText.setText(checkOut.getOrderDate());
        billPriceText.setText(checkOut.getmCheckOutTotal().toString() + "$ CAD");
        updateTextViewTotal(delivery_charge,coupon,tipEditText);
        orderTimeText.setText(checkOut.getOrderTime().toString());
        tip.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    tipEditText = tip.getText().toString();
                    if(tipEditText.isEmpty()) {
                        tipEditText = "0";
                    }
                    updateTextViewTotal(delivery_charge,coupon,tipEditText);
                }
                return false;
            }
        });


        String discuoutValue;


        //TODO: Get Coupon code and price.


        placeOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent paymentIntent = new Intent(PassLoginActivity.this, CreditPayment.class);
//                paymentIntent.putExtra("accountNumber", accountNumber);
//                paymentIntent.putExtra("email", email);
//                paymentIntent.putExtra("phone", phoneNumber);
//                paymentIntent.putExtra("type",orderType);
//                paymentIntent.putExtra("discountCode", discountCodeTxt.getText().toString());

                HashMap<String,Integer> itemExtra = new HashMap<>();
//                Object[] cartItem = cart.getIdProductExtraList().values().toArray();
                Object[] extra = checkOut.getIdProductExtraList().values().toArray();
                Object extra1 = checkOut.getIdProductExtraList().values().iterator().next().iterator().next().getExtraId();
                Log.d("Extra values are ", extra1.toString());
                Log.d("Extra values are ", extra.toString());

               //TODO: Add products list here

                HashMap<Saleable, List<ExtraProuct>> cartItem =  cart.getProductExtraList();
//                Integer ProductId = cartItem.keySet().iterator().next().getproductId();
                List<Object[]> productextras = new ArrayList<>();
                List<Integer> extras = new ArrayList<>();

                List<Integer> pids = new ArrayList<>();

                for(Saleable key: cartItem.keySet()){
                    Integer pid = key.getproductId();
                    pids.add(pid);
                }
                for(List<ExtraProuct> extralist : cartItem.values()) {
                    for(ExtraProuct list: extralist){
                        if(list.getExtraId() != 0 ){
                            extras.add(list.getExtraId());
                        }
                    }
                    productextras.add(extras.toArray());
                    extras.clear();

                }

                List<Integer> quantities = cart.getProductQuantities();


                //               get tip value





                //Submitting Order
                ArrayList<OrderSend.Item> items = new ArrayList<>();
                for(int i=0; i < pids.size();i++){

                    items.add(new OrderSend.Item(String.valueOf(pids.get(i)), quantities.get(i).toString(), Integer.valueOf(tipEditText),productextras.get(i)));
                }


                String strDate = checkOut.getOrderDate();
                try {
                    SimpleDateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
                    SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy");
                    Date date = formate.parse(strDate);
                    dateToString = output.format(date);
                    Log.d("Date is : ", date.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //TODO : Check weather the user Loged in or guest and save its data


                //TODO: Get User data here

                if(accountNumber == "Guest") {
                    //Guest Parameters for Delivery
                    firstname = SaveSharedPreference.getGuestfname(getApplicationContext());
                    lastName = SaveSharedPreference.getGuestlname(getApplicationContext());
                    phoneNumber = SaveSharedPreference.getGuestphone(getApplicationContext());
                    address = SaveSharedPreference.getGuestAddress(getApplicationContext());
                    address1 = SaveSharedPreference.getGuestaddress1(getApplicationContext());
                    postCode= SaveSharedPreference.getGuestPostcode(getApplicationContext());
                    city = SaveSharedPreference.getGuestcity(getApplicationContext());
                    province = SaveSharedPreference.getGuestProvince(getApplicationContext());
                    if (checkOut.getDeliveryIsDifferent() == true) {
                        deliveryAddreess =checkOut.getDeliveryAddress();
                        deliveryAddress2 = checkOut.getDeliveryAddress1();
                        deliverypostCode= checkOut.getDeliveryPostcode();
                        deliveryCity = checkOut.getDeliveryCity();
                        deliveryProvince = checkOut.getDeliveryProvince();
                        companyName = checkOut.getDeliveryCompanyName();
                        companyEmail= checkOut.getDeliveryCompanyEmail();
                    }
                }else {
                    if(checkOut.getDeliveryIsDifferent() == true) {
                        deliveryAddreess = checkOut.getDeliveryAddress();
                        deliveryAddress2 = checkOut.getDeliveryAddress1();
                        deliverypostCode = checkOut.getDeliveryPostcode();
                        deliveryCity = checkOut.getDeliveryCity();
                        deliveryProvince = checkOut.getDeliveryProvince();
                        companyName = checkOut.getDeliveryCompanyName();
                        companyEmail= checkOut.getDeliveryCompanyEmail();
                        user = accountNumber;

                    }else {
                        user = accountNumber;

                    }
                }

//                tipValue = Double.valueOf(tipEditText);
                OrderSend order = new OrderSend(
                        delivery_charge,
                        checkOut.getProviderAddress().getId(),
                        checkOut.getCheckoutInstruction(),
                        checkOut.getOrderTime(),
                        dateToString,
                        orderType,
                        items,
                        coupon,
                        Integer.valueOf(user),
                        firstName,
                        lastName,
                        address,
                        address1,
                        city,
                        province,
                        postCode,
                        phoneNumber,
                        deliveryAddreess,
                        deliveryAddress2,
                        deliverypostCode,
                        deliveryProvince,
                        deliveryCity,
                        companyName,
                        companyEmail,
                        checkOut.getBillingCompany(),
                        checkOut.getBillingEmail()


                );








//                paymentIntent.putExtra("totalAfterDiscount", checkOut.getmCheckoutTotalAfterDiscount().toString());
                sendNetworkRequest(order);

            }
        });

        couponDiscountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Add discuount Feature and detuct the amount from the total
                Log.d("Discount code is:", String.valueOf(discountCodeTxt));
                final String dicountCodetext =  discountCodeTxt.getText().toString();
                // Response received from the server
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean valid = jsonResponse.getBoolean("validity");


                            if (valid) {
                                //TODO: Add Coupon Code
                                coupon = dicountCodetext;
                                String type = jsonResponse.getString("type");
                                String discount = jsonResponse.getString("discount");
                                BigDecimal discountValue = BigDecimal.valueOf(Double.valueOf(discount));
                                Toast.makeText(PassLoginActivity.this,discount + "Type " + type, Toast.LENGTH_LONG).show();
                                if (type.contentEquals("amount")){
                                    checkOut.setmCheckoutTotalAfterDiscount(checkOut.getmCheckOutTotal().subtract(BigDecimal.valueOf(Double.valueOf(discount))));
                                    totalPaymentText.setText(checkOut.getmCheckoutTotalAfterDiscount().toString() + "$ CAD");
                                }else {

                                    checkOut.setmCheckoutTotalAfterDiscount(checkOut.getmCheckOutTotal().multiply(discountValue).divide(BigDecimal.valueOf(100)));
                                    totalPaymentText.setText(checkOut.getmCheckoutTotalAfterDiscount().toString() + "$ CAD");
                                }
                            } else {
                                checkOut.setmCheckoutTotalAfterDiscount(checkOut.getmCheckOutTotal());
                                AlertDialog.Builder builder = new AlertDialog.Builder(PassLoginActivity.this);
                                builder.setMessage("Code is not Valid")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };
                discountRequest request = new discountRequest(dicountCodetext,responseListener);
                RequestQueue queue = Volley.newRequestQueue(PassLoginActivity.this);
                queue.add(request);

            }
        });



    }

    protected void showAlertMessage(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please enter different address for delivery")
                .setPositiveButton(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(PassLoginActivity.this, AddressActivity.class);
                        startActivity(intent);
                    }
                }
                ).create()
                .show();
    }

    private void sendNetworkRequest(OrderSend orderSend){
        //create Okhttp client
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okhttpClientBuilder.addInterceptor(logging);
        //create Retrofit Instance
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://demo.online-menu.ca/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okhttpClientBuilder.build());

        Retrofit retrofit = builder.build();

        // get client & call object for request
        OrderSendClient client = retrofit.create(OrderSendClient.class);

        PostProduct postProduct = new PostProduct();
        Log.d("Sending Data", orderSend.toString());
//        Call<PostProduct> total1 = client.sendOrder(orderSend);
//        total1.enqueue(new Callback<PostProduct>() {
//            @Override
//            public void onResponse(Call<PostProduct> call, Response<PostProduct> response) {
//                Log.d("Sucess",response.message());
//            }
//
//            @Override
//            public void onFailure(Call<PostProduct> call, Throwable t) {
//                Log.d("Failure",t.getMessage());
//            }
//        });


//        Call<PostProduct> total = client.sendOrder1(
//                orderSend.getDate(),
//                orderSend.getDeliveryCharge(),
//                orderSend.getFrom(),
//                (ArrayList<OrderSend.Item>) orderSend.getItems(),
//                orderSend.getNote(),
//                orderSend.getTime(),
//                orderSend.getType());
//        total.enqueue(new Callback<PostProduct>() {
//            @Override
//            public void onResponse(Call<PostProduct> call, Response<PostProduct> response) {
//                Log.d("Sucess", response.message().toString());
//
//                Toast.makeText(CreditPayment.this,response.body().getMessage(), Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onFailure(Call<PostProduct> call, Throwable t) {
//                Log.d("Failure", t.getMessage());
//            }
//        });

//        Map<String,String> map = new HashMap<>();
//        map.put("date",orderSend.getDate());
//        map.put("delivery_charge", orderSend.getDeliveryCharge());
//        map.put("from", orderSend.getFrom());
//        map.put("note", orderSend.getNote());
//        map.put("time", orderSend.getTime());
//        map.put("type", orderSend.getType());
//        Map<String,List<OrderSend.Item>> map2 = new HashMap<>();
//        map2.put("items", orderSend.getItems());

//        Call<PostProduct> total3 = client.sendOrder3(map,map2);
//        total3.enqueue(new Callback<PostProduct>() {
//            @Override
//            public void onResponse(Call<PostProduct> call, Response<PostProduct> response) {
//                Log.d("Success", "It Worked");
//            }
//
//            @Override
//            public void onFailure(Call<PostProduct> call, Throwable t) {
//                Log.d("Failure", "It didn't Worked");
//            }
//        }



        Call<PostProduct> call2 = client.sendOrder2(orderSend.getDate(),orderSend.getDeliveryCharge(),orderSend.getFrom(),
                orderSend.getItems(),orderSend.getNote(),orderSend.getTime(),orderSend.getType(),
                orderSend.getCoupon(),orderSend.getUserId(),orderSend.getBfname(),orderSend.getBlname(),
                orderSend.getBaddress(),orderSend.getBaddress1(),orderSend.getBcity(),orderSend.getBstate(),orderSend.getBpostcode(),orderSend.getBphone(),
                orderSend.getBcompany(),orderSend.getBemail(),"","",orderSend.getdAddress()
                ,orderSend.getdAddress1(),orderSend.getdCity(),orderSend.getdProvince(),orderSend.getdPostcode(),orderSend.getDcompany(),orderSend.getDemail());
        //TODO: Adding first name and last name with the shipping order



        call2.enqueue(new Callback<PostProduct>() {
            @Override
            public void onResponse(Call<PostProduct> call, retrofit2.Response<PostProduct> response) {
                Log.d("Success", "it worked");
                Log.d("Databack", response.body().toString());
                orderNumber = response.body().getOid().toString();
                total = response.body().getAmount().toString();
                Intent payment = new Intent(PassLoginActivity.this, CreditPayment.class);
                payment.putExtra("total", total);
                payment.putExtra("orderNumber", orderNumber);
                PassLoginActivity.this.startActivity(payment);

            }

            @Override
            public void onFailure(Call<PostProduct> call, Throwable t) {
                Log.d("Failure", "it didn't worked");
            }
        });
    }

    private void updateTextViewTotal(String deliveryValue,String coupon,String tipEditText) {

        TextView totalPaymentText = (TextView) findViewById(R.id.billTotalPayment);
        CheckOut checkOut = CheckoutHelper.getCheckout();
        tipValue = Integer.valueOf(tipEditText);
        BigDecimal total = checkOut.getmCheckOutTotal().add(BigDecimal.valueOf(Double.valueOf(deliveryValue)));
        BigDecimal totalWithTip = total.add(BigDecimal.valueOf(tipValue));
        BigDecimal totalWithCoupon = totalWithTip.subtract(BigDecimal.valueOf(Double.valueOf(coupon)));

        totalPaymentText.setText(String.valueOf(totalWithCoupon.doubleValue()) +"$ CAD");
    }



}
