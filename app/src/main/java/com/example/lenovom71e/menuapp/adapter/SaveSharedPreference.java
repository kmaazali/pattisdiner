package com.example.lenovom71e.menuapp.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by admin on 2017-08-23.
 */

public class SaveSharedPreference {
    static final String PREF_USER_NAME = "username";
    static final String PREF_PASSWORD = "password";
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static void setAccountAsGuest(Context ctx, Boolean isGuest){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putBoolean("isGuest", isGuest);
        editor.commit();
    }
    public static boolean getAccountAsGuest(Context ctx){
        return getSharedPreferences(ctx).getBoolean("isGuest", false);
    }

    public static void setGuestInfo(Context ctx, String fname, String lName,String phone, String address,
                                    String address2, String city, String province, String postalCode){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString("fname", fname);
        editor.putString("lname", lName);
        editor.putString("address", address);
        editor.putString("address2", address2);
        editor.putString("city", city);
        editor.putString("provice", province);
        editor.putString("postCode", postalCode);
        editor.putString("phone", phone);
        editor.commit();
    }

    public static String getGuestfname(Context ctx){
        return getSharedPreferences(ctx).getString("fname", "");
    }

    public static String getGuestlname(Context ctx){
        return getSharedPreferences(ctx).getString("lname", "");
    }

    public static String getGuestAddress(Context ctx){
        return getSharedPreferences(ctx).getString("address", "");
    }
    public static String getGuestaddress1(Context ctx){
        return getSharedPreferences(ctx).getString("address2", "");
    }
    public static String getGuestcity(Context ctx){
        return getSharedPreferences(ctx).getString("city", "");
    }
    public static String getGuestProvince(Context ctx){
        return getSharedPreferences(ctx).getString("provice", "");
    }
    public static String getGuestPostcode(Context ctx){
        return getSharedPreferences(ctx).getString("postCode", "");
    }
    public static String getGuestphone(Context ctx){
        return getSharedPreferences(ctx).getString("phone", "");
    }


    public static void setPassword(Context ctx, String password){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_PASSWORD, password);
        editor.commit();
    }

    public static String getPassword(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_PASSWORD, "");
    }

    public static  String getUserName(Context ctx){
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void clearUserName(Context ctx) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear();
        editor.commit();
    }
}
