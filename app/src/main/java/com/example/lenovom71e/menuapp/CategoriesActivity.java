package com.example.lenovom71e.menuapp;

/**
 * Created by STUDENT on 2/17/2016.
 */
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.coboltforge.slidemenu.SlideMenu;
import com.coboltforge.slidemenu.SlideMenuInterface;
import com.example.lenovom71e.menuapp.adapter.CategoryListAdapter;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.Category;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends AppCompatActivity implements SlideMenuInterface.OnSlideMenuItemClickListener {
    // Log tag
    private static final String TAG = MainActivity.class.getSimpleName();
    // Categories json url
    private ProgressDialog pDialog;
    private List<Category> categoryList = new ArrayList<Category>();
    private CategoryListAdapter adapter;
//    private ProgressDialog pDialog;
    private SlideMenu slidemenu;
    private final static int MYITEMID = 42;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity);
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setTitle("Categories");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.menu_button);
        toolbar.setLogo(R.mipmap.logo);
        slidemenu = (SlideMenu) findViewById(R.id.slideMenu);
        slidemenu.init(this, R.menu.slide, this, 333);
        slidemenu.setHeaderImage(getResources().getDrawable(R.mipmap.logo));
        SlideMenu.SlideMenuItem item = new SlideMenu.SlideMenuItem();
        slidemenu.addMenuItem(item);

        final String url = this.getResources().getString(R.string.api_url)+"categories";

        ListView listView = (ListView) findViewById(R.id.list);
        adapter = new CategoryListAdapter(this, categoryList);
        listView.setAdapter(adapter);
        //listView.setAdapter(new CategoryListAdapter(this, categoryList));

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();
        // Creating volley request obj
        JsonArrayRequest catReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        // get listview
                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Category category = new Category();
                                category.setId(obj.getInt("idcategory"));
                                category.setCategory(obj.getString("name"));
                                // adding categories to categories array
                                categoryList.add(category);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        hidePDialog();
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();

                    }
                }
        );

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Intent i = new Intent(getApplicationContext(), ProductActivity.class);
                // send category id to productlist activity to get list of products under that category
                String category_id = ((TextView) view.findViewById(R.id.category_id)).getText().toString();
                String category_name = ((TextView) view.findViewById(R.id.category_name)).getText().toString();
                i.putExtra("category_id", category_id);
                i.putExtra("category_name", category_name);
                startActivity(i);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(catReq);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onSlideMenuItemClick(int itemId) {
        switch (itemId) {
            case R.id.item_one:
                Intent p = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(p);
                break;
            case R.id.item_two:
                Intent j = new Intent(getApplicationContext(), CategoriesActivity.class);
                startActivity(j);
                break;
//            case R.id.item_three:
//                Intent k = new Intent(getApplicationContext(), MainActivity.class);
//                startActivity(k);
//                break;
            case MYITEMID:
                Toast.makeText(this, "Dynamically added item selected", Toast.LENGTH_SHORT).show();
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Profile profile = ProfileHelper.getProfilehelper();
        switch (item.getItemId()) {
            case android.R.id.home: // this is the app icon of the actionbar
                slidemenu.show();
                break;
            case R.id.action_user:
                Intent intent = new Intent(CategoriesActivity.this, UserProfile.class);
                profile.setFromActivity("categories");
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}