package com.example.lenovom71e.menuapp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;
import com.example.lenovom71e.menuapp.util.RegisterDeliveryActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        final TextView tvRegisterLink = (TextView) findViewById(R.id.tvRegisterLink);
        final Button bLogin = (Button) findViewById(R.id.bSignIn);

        final CheckOut checkOut = CheckoutHelper.getCheckout();
        final TextView signInAsGuest = (TextView) findViewById(R.id.userGuestLogin);
        final Profile profile = ProfileHelper.getProfilehelper();

        if (SaveSharedPreference.getAccountAsGuest(getApplicationContext()) == true){
            Intent intent = new Intent(LoginActivity.this, PassLoginActivity.class);
            startActivity(intent);
        }else {
            etUsername.setText(SaveSharedPreference.getUserName(getApplicationContext()));
            etPassword.setText(SaveSharedPreference.getPassword(getApplicationContext()));

//            Intent intent = new Intent(LoginActivity.this, PassLoginActivity.class);
//            startActivity(intent);

        }

        signInAsGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: login into guestLogin Activity

                Intent intent = new Intent(LoginActivity.this, GuestLoginActivity.class);
                startActivity(intent);
            }
        });

        tvRegisterLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (checkOut.getOrderTypeId()){
                    case 1:
                        //Delivery Case
                        Intent registerDeliveryIntent = new Intent(LoginActivity.this, RegisterDeliveryActivity.class);
                        LoginActivity.this.startActivity(registerDeliveryIntent);
                        break;
                    case 2:
                    case 3:
                        Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                        LoginActivity.this.startActivity(registerIntent);
                        break;
                }


            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = etUsername.getText().toString();
                final String password = etPassword.getText().toString();
                SaveSharedPreference.setPassword(getApplicationContext(),password);
                SaveSharedPreference.setUserName(getApplicationContext(),username);

                Log.d("Loged in USer", SaveSharedPreference.getUserName(getApplicationContext()).toString());

                // Response received from the server
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");


                            if (success) {

                                String userId = jsonResponse.getString("user_id");
                                String firstName= jsonResponse.getString("first_name");
                                String lastName = jsonResponse.getString("last_name");
                                String loginEmail = jsonResponse.getString("email");
                                String phone = jsonResponse.getString("phone");
                                String postCode = jsonResponse.getString("postcode");
                                String billingCompany = jsonResponse.getString("company");
                                String billingEmail = jsonResponse.getString("email");
                                String city = jsonResponse.getString("city");
                                String province = jsonResponse.getString("province");


                                //String name = jsonResponse.getString("name");
                                //int age = jsonResponse.getInt("age");
                                profile.setFirstName(firstName);
                                profile.setLastName(lastName);
                                profile.setLoginEmail(loginEmail);
                                profile.setPhone(phone);
                                profile.setBillingCompany(billingCompany);
                                profile.setBillingEmail(billingEmail);
                                profile.setAddress(jsonResponse.getString("address"));
                                profile.setCiry(city);
                                profile.setProvince(province);
                                profile.setUserId(userId);
                                profile.setPostCode(postCode);
                                Boolean isUserProfile = getIntent().getBooleanExtra("isUserProfile",false);
                                if (isUserProfile == true) {
                                    Intent intent = new Intent(LoginActivity.this, UserProfile.class);
                                    startActivity(intent);
                                }else {


                                    Intent intent = new Intent(LoginActivity.this, PassLoginActivity.class);
                                    intent.putExtra("name", firstName);
                                    intent.putExtra("lastName", lastName);
                                    intent.putExtra("userId", userId);
                                    intent.putExtra("phone", phone);
                                    intent.putExtra("email", loginEmail);
                                    intent.putExtra("postcode",postCode);
                                    checkOut.setBillingCompany(billingCompany);
                                    checkOut.setBillingEmail(billingEmail);
                                    checkOut.setUserId(userId);


                                    LoginActivity.this.startActivity(intent);
                                }
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage("Login Failed")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                LoginRequest loginRequest = new LoginRequest(username, password, responseListener);
                RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                queue.add(loginRequest);
                CheckOut checkOut = CheckoutHelper.getCheckout();

            }
        });
    }

    @Override
    public void onBackPressed(){

    }
}

