package com.example.lenovom71e.menuapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;

public class GuestLoginActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_login);

        final EditText firstName = (EditText) findViewById(R.id.guestFname);
        final EditText lastName = (EditText) findViewById(R.id.guestLname);
        final EditText address = (EditText) findViewById(R.id.guestAddress);
        final EditText address2 = (EditText) findViewById(R.id.guestAddress2);
        final EditText city= (EditText) findViewById(R.id.guestCity);
        final EditText province = (EditText) findViewById(R.id.guestProvince);
        final EditText postCode= (EditText) findViewById(R.id.guestPostCode);
        final EditText phone = (EditText) findViewById(R.id.guestPhone);
        Button guestLogin = (Button) findViewById(R.id.guestLoginBtn);
        final Profile profile = ProfileHelper.getProfilehelper();



        guestLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fName, lName, addressText, address2Txt, citytxt, provinceTxt, postCodeTxt, phoneTxt;
                fName = firstName.getText().toString();
                lName = lastName.getText().toString();
                addressText = address.getText().toString();
                address2Txt = address2.getText().toString();
                citytxt = city.getText().toString();
                provinceTxt = province.getText().toString();
                postCodeTxt = postCode.getText().toString();
                phoneTxt = phone.getText().toString();

                if (checkRequired(fName) && checkRequired(lName) && checkRequired(addressText)
                        && checkRequired(citytxt) && checkRequired(provinceTxt) && checkRequired(postCodeTxt) && checkRequired(phoneTxt) ){

                    SaveSharedPreference.setGuestInfo(getApplicationContext(),fName,lName,phoneTxt,addressText,
                            address2Txt,citytxt,provinceTxt,postCodeTxt);
                    SaveSharedPreference.setAccountAsGuest(getApplicationContext(), true);
                    Intent intent = new Intent(GuestLoginActivity.this, PassLoginActivity.class);
                    intent.putExtra("name", fName);
                    intent.putExtra("lastName", lName);
                    intent.putExtra("userId", "Guest");
                    intent.putExtra("phone", phoneTxt);
                    intent.putExtra("email", "None");
                    intent.putExtra("postcode",postCodeTxt);
                    Boolean isUserProfile = getIntent().getBooleanExtra("isUserProfile",false);
                    if (isUserProfile == true) {
                        Intent intenta = new Intent(GuestLoginActivity.this, UserProfile.class);
                        startActivity(intenta);
                    }
                    startActivity(intent);
                }

            }
        });




    }

    public boolean checkRequired(String input){
        boolean done = false;
        if (input.length() == 0) {
            disPlayMessage("Please Enter all required Fields");
        }else {
            done = true;
        }
        return done;
    }

    public void disPlayMessage(String message){
        AlertDialog builder = new AlertDialog.Builder(this)
                .setMessage(message)
                .setNegativeButton("Okay",null)
                .create();
        builder.show();
    }
}
