package com.example.lenovom71e.menuapp.adapter;

/**
 * Created by LENOVO on 3/18/2016.
 */
import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.model.Choice;
import com.example.lenovom71e.menuapp.model.Extra;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static android.widget.Toast.LENGTH_SHORT;

public class ExpandListAdapter extends BaseExpandableListAdapter {
    private Context context;
    //private LayoutInflater inflater;
    private ArrayList<Extra> groups;
    private LayoutInflater infalInflater;
    // private int[] colors = new int[] { 0x302C6700, 0x30FFCC00 };
    private final Set<Pair<Long, Long>> mCheckedItems = new HashSet<Pair<Long, Long>>();
    int selected_position =-1;
    Toast toastOject;
    public Set<Pair<Long, Long>> getCheckedItems() {
        return mCheckedItems;
    }
    public ExpandListAdapter(Context context, ArrayList<Extra> groups) {
        this.context = context;
        this.groups = groups;
        this.infalInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<Choice> chList = groups.get(groupPosition).getChoices();
        return chList.get(childPosition);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override

    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        Choice child = (Choice) getChild(groupPosition, childPosition);

        int mul = child.getCmultiple();


        if (convertView == null) {

         convertView = infalInflater.inflate(R.layout.choice_extra_check, null);
        }
                    final CheckBox cb = (CheckBox) convertView.findViewById(R.id.cbBox);
                    cb.setText(child.getCname().toString());
                    TextView price = (TextView) convertView.findViewById(R.id.choice_price);
                    BigDecimal Choiceprice = child.getCprice();
                    price.setText("+ C$" + String.valueOf(Choiceprice));

        switch (mul) {
            case 0:
                if (selected_position == childPosition) {
                    cb.setChecked(true);
//                    Toast.makeText(context, "this item has been selected" + child.getCname().toString(), Toast.LENGTH_LONG).show();
                } else {
                    cb.setChecked(false);


                    if (toastOject != null) {
                        toastOject.cancel();
                    }
                    toastOject = Toast.makeText(context, "You can choose only one", LENGTH_SHORT);
                    toastOject.show();
                }

                final Pair<Long, Long> tag = new Pair<Long, Long>(
                       getGroupId(groupPosition),
                      getChildId(groupPosition, childPosition));

                cb.setTag(tag);
                cb.setOnClickListener(new View.OnClickListener() {



                    @Override
                   public void onClick(View view) {


                        final Pair<Long, Long> tag = (Pair<Long, Long>) view.getTag();

                        if (((CheckBox) view).isChecked()) {

                            selected_position = childPosition;
                            mCheckedItems.add(tag);
                            //Toast.makeText(context, String.valueOf(tag),Toast.LENGTH_SHORT).show();
                            Log.d("item checked", "this item has been clicked");

                        } else {
                            selected_position = -1;
                           mCheckedItems.remove(tag);
                        }
                        notifyDataSetChanged();

                    }
                });

                //cb.setChecked(mCheckedItems.contains(tag));

                break;
            case 1:
                final Pair<Long, Long> tag1 = new Pair<Long, Long>(
                        getGroupId(groupPosition),
                        getChildId(groupPosition, childPosition));
                cb.setTag(tag1);
                cb.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        final Pair<Long, Long> tag = (Pair<Long, Long>) v.getTag();
                        if (((CheckBox) v).isChecked()) {
                            mCheckedItems.add(tag);
                        } else {
                            mCheckedItems.remove(tag);
                        }
                        notifyDataSetChanged();
                    }
                });
                cb.setChecked(mCheckedItems.contains(tag1));
                break;

        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Choice> chList = groups.get(groupPosition).getChoices();
        return chList.size();
    }
    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Extra group = (Extra) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.list_extra, null);
        }


        TextView tv = (TextView) convertView.findViewById(R.id.textExtras);
        tv.setText(group.getEname());
        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
    // void showToastMessage(String message) {
    //Toast.makeText(context, message, Toast.LENGTH_SHORT)
    //.show();

    // }



}

