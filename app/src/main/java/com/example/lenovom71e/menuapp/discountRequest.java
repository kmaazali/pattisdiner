package com.example.lenovom71e.menuapp;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017-08-22.
 */

public class discountRequest extends StringRequest{


        private static String DISCOUNTURL = "https://demo.online-menu.ca/api/discount/";
        private Map<String, String> params;
        public discountRequest(String discountCode , Response.Listener<String> listener){
            super(Method.GET, DISCOUNTURL + discountCode, listener, null);

            params = new HashMap<>();
            params.put("code", discountCode);

        }

        @Override
    public Map<String,String> getParams(){
            return params;
        }


}
