package com.example.lenovom71e.menuapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.lenovom71e.menuapp.Service.OrderSendClient;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Paymentconfirm;
import com.example.lenovom71e.menuapp.model.PostProduct;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreditPayment extends AppCompatActivity {

    final CheckOut checkout = CheckoutHelper.getCheckout();
    String accountNumber, email, phone, totalAfterDiscount;
    static final String PAYMENTPOSTURL = "https://demo.online-menu.ca/api/payment";
//    RequestQueue queue = Volley.newRequestQueue(this);
    String orderNumberString = "";
    String total = "";
    String fullname="", card_number="", expirydate="",cvc="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_payment);
        final RequestQueue queue = Volley.newRequestQueue(this);

        final TextView fullName = (TextView) findViewById(R.id.fullName);
        final TextView cardNumber = (TextView) findViewById(R.id.creditCardNumber);
        final TextView expiry = (TextView) findViewById(R.id.expiryDate);
        final TextView cvcNumber = (TextView) findViewById(R.id.cvcNumber);

        final Button placeOrderBtn = (Button) findViewById(R.id.placeOrderBtn);
        final TextView orderNumberTxt = (TextView) findViewById(R.id.paymentOrderNumberTxt);
        ImageView imageLogo = (ImageView) findViewById(R.id.logoImage);
//        accountNumber = getIntent().getStringExtra("accountNumber");
//        email = getIntent().getStringExtra("email");
//        phone = getIntent().getStringExtra("phone");
        orderNumberString = getIntent().getStringExtra("orderNumber");
        total = getIntent().getStringExtra("total");
        totalAfterDiscount = checkout.getmCheckoutTotalAfterDiscount().toString();

        orderNumberTxt.setText(orderNumberString);




        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.myanimation);
        imageLogo.startAnimation(animation);


        final TextView priceText = (TextView) findViewById(R.id.paymentBalanceTxt);
        priceText.setText(totalAfterDiscount.trim().toString() + "$ CAD");




        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fullname = fullName.getText().toString();
                card_number = cardNumber.getText().toString();
                expirydate = expiry.getText().toString();
                cvc = cvcNumber.getText().toString();

                if(fullname.isEmpty() || card_number.isEmpty() || expirydate.isEmpty()||cvc.isEmpty()){
                    Toast.makeText(CreditPayment.this,"please fill all infomraion",Toast.LENGTH_LONG).show();
                }
                sendNetworkRequest(orderNumberString,total,card_number,expirydate,cvc);

            }
        });




    }

    private void serializeOrderData(){
        List<Integer> extras = new ArrayList<Integer>();
        extras.add(5);
        extras.add(21);
        extras.add(18);

    }



    private void sendNetworkRequest(String orderNumberString,String total,String card_number,String expirydate,String cvc){
        //create Okhttp client
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        okhttpClientBuilder.addInterceptor(logging);
        //create Retrofit Instance
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://demo.online-menu.ca/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okhttpClientBuilder.build());

        Retrofit retrofit = builder.build();

        // get client & call object for request
        OrderSendClient client = retrofit.create(OrderSendClient.class);

        PostProduct postProduct = new PostProduct();
//        Log.d("Sending Data", orderSend.toString());
//        Call<PostProduct> total1 = client.sendOrder(orderSend);
//        total1.enqueue(new Callback<PostProduct>() {
//            @Override
//            public void onResponse(Call<PostProduct> call, Response<PostProduct> response) {
//                Log.d("Sucess",response.message());
//            }
//
//            @Override
//            public void onFailure(Call<PostProduct> call, Throwable t) {
//                Log.d("Failure",t.getMessage());
//            }
//        });


//        Call<PostProduct> total = client.sendOrder1(
//                orderSend.getDate(),
//                orderSend.getDeliveryCharge(),
//                orderSend.getFrom(),
//                (ArrayList<OrderSend.Item>) orderSend.getItems(),
//                orderSend.getNote(),
//                orderSend.getTime(),
//                orderSend.getType());
//        total.enqueue(new Callback<PostProduct>() {
//            @Override
//            public void onResponse(Call<PostProduct> call, Response<PostProduct> response) {
//                Log.d("Sucess", response.message().toString());
//
//                Toast.makeText(CreditPayment.this,response.body().getMessage(), Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onFailure(Call<PostProduct> call, Throwable t) {
//                Log.d("Failure", t.getMessage());
//            }
//        });

//        Map<String,String> map = new HashMap<>();
//        map.put("date",orderSend.getDate());
//        map.put("delivery_charge", orderSend.getDeliveryCharge());
//        map.put("from", orderSend.getFrom());
//        map.put("note", orderSend.getNote());
//        map.put("time", orderSend.getTime());
//        map.put("type", orderSend.getType());
//        Map<String,List<OrderSend.Item>> map2 = new HashMap<>();
//        map2.put("items", orderSend.getItems());

//        Call<PostProduct> total3 = client.sendOrder3(map,map2);
//        total3.enqueue(new Callback<PostProduct>() {
//            @Override
//            public void onResponse(Call<PostProduct> call, Response<PostProduct> response) {
//                Log.d("Success", "It Worked");
//            }
//
//            @Override
//            public void onFailure(Call<PostProduct> call, Throwable t) {
//                Log.d("Failure", "It didn't Worked");
//            }
//        });
//        Call<PostProduct> call2 = client.sendOrder2( orderSend.getDate(),orderSend.getDeliveryCharge(),orderSend.getFrom(),  orderSend.getItems(),orderSend.getNote(),orderSend.getTime(),orderSend.getType()
//                , orderSend.getCoupon(),orderSend.getUserId(),orderSend.getBfname(),orderSend.getBlname(),orderSend.getBaddress(),orderSend.getBaddress1(),orderSend.getBcity(),orderSend.getBstate(),orderSend.getBpostcode(),orderSend.getBphone()
//                ,orderSend.getdAddress(),orderSend.getdAddress1(),orderSend.getdCity(),orderSend.getdProvince(),orderSend.getdPostcode());
//        orderNumberString,total,card_number,expirydate,cvc
        StringBuilder message = new StringBuilder();
        if(card_number.isEmpty() && expirydate.isEmpty() && cvc.isEmpty() && fullname.isEmpty()){

            if (card_number.isEmpty() && card_number.length() != 16){
                message.append("Please Enter Card Number or the correct credit card number \n");

//                cardNumber.requestFocus();
            }
            if (fullname.isEmpty()){
                message.append("Please enter your full name on the credit card \n");

//                cardNumber.requestFocus();
            }
            if(expirydate.isEmpty() && expirydate.length() != 4 ){
                message.append("Please enter the four digits expiry date \n");
            }
            if(cvc.isEmpty() && cvc.length() != 3){
                message.append("Please enter 3 digits of the CVC that is on the back of your card \n");
            }
            viewNegativeMessage(message.toString());


        }else {
            int orderId = Integer.parseInt(orderNumberString);
            Double totalToDouble = Double.valueOf(total);
            long cardNumber = Long.parseLong(card_number);
//        int cardNumber = Integer.parseInt(card_number);
            int expiryDate = Integer.parseInt(expirydate);
            Integer cvcNumber = Integer.parseInt(cvc);
            Call<Paymentconfirm> call2 = client.makePayment(orderId,totalToDouble, fullname,cardNumber,expirydate,cvcNumber);
            call2.enqueue(new Callback<Paymentconfirm>() {
                @Override
                public void onResponse(Call<Paymentconfirm> call, Response<Paymentconfirm> response) {
                    Log.d("Success", "it worked");
                    Log.d("Databack", response.body().toString());
                    Paymentconfirm confirm = response.body();
                    if(response.body().isStatus() == true) {

                        viewMessageStatus(response.body().getData().getSslResult());
                    }else {
                        viewNegativeMessage(response.body().getData().getSslResult());
                    }

                }

                @Override
                public void onFailure(Call<Paymentconfirm> call, Throwable t) {
                    Log.d("Failure", "it didn't worked");

                }
            });

        }

//        call2.enqueue(new Callback<PostProduct>() {
//            @Override
//            public void onResponse(Call<PostProduct> call, Response<PostProduct> response) {
//                Log.d("Success", "it worked");
//                Log.d("Databack", response.body().toString());
//            }
//
//            @Override
//            public void onFailure(Call<PostProduct> call, Throwable t) {
//                Log.d("Failure", "it didn't worked");
//            }
//        });
    }

    public void viewMessageStatus(String status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("PaymentProccess " + status);
        builder.setPositiveButton("Thank you", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(CreditPayment.this, CategoriesActivity.class);
                startActivity(intent);
            }
        });



    }

    public void viewNegativeMessage(String status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(status);
        builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }



}
