package com.example.lenovom71e.menuapp;

import android.util.Log;

import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017-10-02.
 */

public class PlaceOrderRequest extends StringRequest {

    //TODO: need to change the request URL
    private static final String PLACE_ORDER_REQUEST_URL =  "https://demo.online-menu.ca/placeorder";
    private Map<String, String> params;
    JSONArray extrass = new JSONArray();
    CheckOut checkoutItem = CheckoutHelper.getCheckout();


    public PlaceOrderRequest(String productId, ArrayList<ExtraProuct> extras, String quantity, Response.Listener<String> listener) {

        super(Method.POST, PLACE_ORDER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("productId", productId);
        params.put("quanitity", quantity);

//        params.put("extras", extrass);

        Log.d("placeOrderLog", checkoutItem.getIdProductExtraList().toString());

    }

}
