package com.example.lenovom71e.menuapp;

/**
 * Created by LENOVO on 12/7/2016.
 */

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class RegisterRequest extends StringRequest {
    private static final String REGISTER_REQUEST_URL = "https://demo.online-menu.ca/api/register";
    //TODO: we have to change REGISTER_REQUEST_URL
    private Map<String, String> params;

    public RegisterRequest(String name,String lastName, String username, String password,
                           String phoneNumber, Response.Listener<String> listener) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("first_name", name);
        params.put("last_name", lastName);
        //params.put("age", age + "");
        params.put("email", username);
        params.put("password", password);
        params.put("phone", phoneNumber);
        params.put("address1", "1");
        params.put("address2", "1");
        params.put("city", "1");
        params.put("province", "1");
        params.put("postcode", "1");
        params.put("country", "1");
        params.put("company", "1");


    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}