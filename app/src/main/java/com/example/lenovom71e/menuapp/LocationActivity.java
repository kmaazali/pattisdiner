package com.example.lenovom71e.menuapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.lenovom71e.menuapp.adapter.LocationAdapter;
import com.example.lenovom71e.menuapp.model.Address;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_SHORT;

public class LocationActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    String JsonURL = "https://demo.online-menu.ca/api/location" ;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);




        final ArrayList<Address> addresses = (ArrayList<Address>) getIntent().getSerializableExtra("extraLocations");

        ListView locationListView = (ListView) findViewById(R.id.locationList);
        LocationAdapter adapter = new LocationAdapter(this,addresses);
        locationListView.setAdapter(adapter);

        locationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(LocationActivity.this, "you just clicked item number : " + position, LENGTH_SHORT).show();
                Intent locationListIntent = new Intent(LocationActivity.this, CheckoutActivity.class);
                locationListIntent.putExtra("companyName", addresses.get(position).getmName());
                locationListIntent.putExtra("companyAddress", addresses.get(position).getmAddress());
                locationListIntent.putExtra("companyCity", addresses.get(position).getmCity());
                locationListIntent.putExtra("companyPhone", addresses.get(position).getmPhoneNumber());
                CheckOut checkout = CheckoutHelper.getCheckout();
                checkout.setProviderAddress(addresses.get(position));

                startActivity(locationListIntent);
            }
        });




    }


}
