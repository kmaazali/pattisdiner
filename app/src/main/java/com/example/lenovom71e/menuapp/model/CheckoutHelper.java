package com.example.lenovom71e.menuapp.model;

/**
 * Created by admin on 2017-07-18.
 */

public class CheckoutHelper {
    private static CheckOut checkout = new CheckOut();


    public static CheckOut getCheckout(){
        if (checkout == null) {
            checkout = new CheckOut();
        }
        return checkout;
    }
}
