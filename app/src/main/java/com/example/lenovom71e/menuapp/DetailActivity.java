package com.example.lenovom71e.menuapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tonyvu.sc.model.Cart;
import com.android.tonyvu.sc.model.ExtraProuct;
import com.android.tonyvu.sc.util.CartHelper;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.coboltforge.slidemenu.SlideMenu;
import com.coboltforge.slidemenu.SlideMenuInterface;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.CartItem;
import com.example.lenovom71e.menuapp.model.Choice;
import com.example.lenovom71e.menuapp.model.Product;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by LENOVO on 3/16/2016.
 */
public class DetailActivity extends AppCompatActivity implements SlideMenuInterface.OnSlideMenuItemClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    // Products json url
    private static final String url = "https://demo.online-menu.ca/api/product/";
    private static final String url1 = "https://demo.online-menu.ca/api/product_extras/";
    private ProgressDialog pDialog;
    private SlideMenu slidemenu;
    private final static int MYITEMID = 42;
    private Toolbar toolbar;
    int minteger = 1;
    String product_id;
    String category_id;
    String pname;
    Product product;
    Choice choice2;
    Choice choice1;
    Spinner spQuantity;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    Button btn1;
    RequestQueue requestQueue;
    private static final String ImageURL = "https://demo.online-menu.ca/assets/img/product/";
    public static final List<Integer> QUANTITY_LIST = new ArrayList<Integer>();
    ArrayList<ExtraProuct> nameExtra2 = new ArrayList<>();
    //Variables needed for ExpandableListView
    ArrayList<String> listDataHeader = new ArrayList<>();
    HashMap<CartItem, ArrayList<String>> extraCart = new HashMap<CartItem, ArrayList<String>>();
    List<CartItem> listDataHeader1 = new ArrayList<>();
    double extraTotalPrice = 0;
    int quantityOrder = 0;
    ExtraProuct rdValue;
    HashMap <String, ExtraProuct> radioButtonList = new HashMap<>();
    ArrayList<Choice> choices1 = new ArrayList<>();
    ArrayList<Choice> choices2 = new ArrayList<>();
    private static final AtomicInteger itemId = new AtomicInteger(0);


    static {
        for (int i = 1; i < 11; i++) QUANTITY_LIST.add(i);
    }

    private int radioButtonGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);
        //Bundle data = getIntent().getExtras();
        //product = (Product) data.getSerializable("product");
        //Log.d(TAG, "Product hashCode: " + product.hashCode());
        spQuantity = (Spinner) findViewById(R.id.spQuantity);
        btn1 = (Button) findViewById(R.id.btnDone);



        requestQueue = Volley.newRequestQueue(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_button);
        toolbar.setLogo(R.mipmap.logo);
        slidemenu = (SlideMenu) findViewById(R.id.slideMenu);
        slidemenu.init(this, R.menu.slide, this, 333);
        slidemenu.setHeaderImage(getResources().getDrawable(R.mipmap.logo));
        SlideMenu.SlideMenuItem item = new SlideMenu.SlideMenuItem();
        slidemenu.addMenuItem(item);
        Intent i = getIntent();
        product_id = i.getStringExtra("product_id");
        category_id = i.getStringExtra("category_id");
        pname = i.getStringExtra("pname");
        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();
        setTitle(pname);
        initializeQuantity();


        JsonObjectRequest obreq = new JsonObjectRequest(url+product_id, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    JSONObject obj = response.getJSONObject("products");
//                    JSONObject obj = response.getJSONObject("");
//                    Log.v("Detail Activity", String.valueOf(obj));
                    product = new Product();
                    TextView cId = (TextView) findViewById(R.id.category_id);
                    int catid = product.setId(obj.getInt("idcategory"));
                    cId.setText(String.valueOf(catid));
                    TextView id = (TextView) findViewById(R.id.product_id);
                    int Pid = product.setPid(obj.getInt("product_id"));
                    id.setText(String.valueOf(Pid));
                    TextView name = (TextView) findViewById(R.id.pname);
                    String Proname = product.setPname(obj.getString("product_name"));
                    name.setText(Proname);
                    TextView description = (TextView) findViewById(R.id.pdescription);
                    String ProDesc = product.setPdescription(obj.getString("product_description"));
                    description.setText(ProDesc);
                    TextView price = (TextView) findViewById(R.id.price);
                    BigDecimal price1 = new BigDecimal(obj.getString("price"));
                    BigDecimal Proprice = product.setPrice(price1);
                    price.setText("C$" + String.valueOf(Proprice));
                    if (imageLoader == null)
                        imageLoader = AppController.getInstance().getImageLoader();
                    NetworkImageView thumbNail = (NetworkImageView) findViewById(R.id.thumbnail);
                    String Proimage = product.setThumbnailUrl(obj.getString("image"));
                    String ProComImage = ImageURL + Proimage;
                    thumbNail.setImageUrl(ProComImage, imageLoader);
                    //detailList.add(product);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
        // The final parameter overrides the method onErrorResponse() and passes VolleyError
        //as a parameter
        new Response.ErrorListener() {
            @Override
            // Handles errors that occur due to Volley
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "Error");
                hidePDialog();
            }
        }
        );

        JsonObjectRequest extraReq = new JsonObjectRequest(Request.Method.GET,url1+product_id,
                (String)null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("BOBBY", response.toString());
                        if (response.length() < 0){


                        }else {

                            try {
                                LinearLayout ll=(LinearLayout)findViewById(R.id.extralist);
                                LinearLayout lTitle = new LinearLayout(DetailActivity.this);
                                lTitle.setOrientation(LinearLayout.HORIZONTAL);
                                ll.addView(lTitle);
                                LinearLayout.LayoutParams params;
                                Iterator<String> key = response.keys();

                                while (key.hasNext()) {
                                    String k = key.next();
                                    //Extra ext = new Extra();

                                    final JSONObject category = response.getJSONObject(k);
//                                if (!category.isNull("extras")){
//                                    Toast.makeText(DetailActivity.this, "There is  Extra",Toast.LENGTH_LONG).show();
//
//                                }else {
//                                    Toast.makeText(DetailActivity.this, "There is NO Extra",Toast.LENGTH_LONG).show();
//                                }
                                    final TextView title = new TextView(DetailActivity.this);
                                    //String Title = ext.setEname(category.getString("category_name"));
                                    title.setText(category.getString("category_name"));
                                    //title.setText(Title);
                                    title.setTextColor(0xff9a1000);
                                    title.setTextSize(18);
                                    title.setTypeface(null, Typeface.BOLD);
                                    ll.addView(title);
                                    final TextView requiredText = new TextView(DetailActivity.this);
                                    requiredText.setText("*Required");
                                    requiredText.setTextSize(12);
                                    requiredText.setTextColor(0xff9a1000);
                                    requiredText.setTypeface(null, Typeface.ITALIC);
                                    int is_required = Integer.parseInt(category.getString("is_required"));
                                    if (is_required == 1) {
                                    ll.addView(requiredText);
                                    }

                                    JSONObject extras = category.getJSONObject("extras");
                                    Iterator<String> extrasiterator = extras.keys();

                                    if(category.getInt("is_multiple_select") == 0) {

                                        radioButtonGroup = 1;
                                        RadioButton[] rb = new RadioButton[extras.length()];
                                        TextView[] price = new TextView[extras.length()];
                                        final RadioGroup rg = new RadioGroup(DetailActivity.this);
                                        rg.setOrientation(RadioGroup.VERTICAL);
                                        int count=0;
                                        final String[] choiceName = new String[count];
                                        while(extrasiterator.hasNext()){
                                            String ek = extrasiterator.next();
                                            JSONObject extra = extras.getJSONObject(ek);
                                            choice1 = new Choice();
                                            int Choid1 = choice1.setIde(extra.getInt("extra_id"));
                                            String name =  choice1.setCname(extra.getString("extra_name"));
//                                        String pricee =  choice1.set(extra.getDouble("extra_price"));
                                            choices1.add(new Choice(Choid1, name));
                                            rb[count] = new RadioButton(DetailActivity.this);
                                            price[count] = new TextView(DetailActivity.this);
                                            params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                                            params.gravity = Gravity.CENTER_HORIZONTAL;
                                            rg.addView(rb[count]);
                                            //rg.addView(price[count]);
                                            //String[] Choname = new String[extra.length()];
                                            String Choname = choice1.setCname(extra.getString("extra_name"));
                                            BigDecimal price1 = new BigDecimal(extra.getString("extra_price"));
                                            BigDecimal Proprice = choice1.setCprice(price1);
                                            String Choprice = String.valueOf(Proprice);
                                            rb[count].setText(Choname+ "    CAD" +Choprice);
                                            //rb[count].setText(extra.getString("extra_name") + "    CAD" +extra.getString("extra_price"));
                                            //rb[count].setText(choice1.setCname(extra.getString("extra_name")));
                                            //BigDecimal price1 = new BigDecimal(extra.getString("extra_price"));
                                            // BigDecimal Proprice = choice1.setCprice(price1);
                                            // price[count].setText("CAD" + extra.getString("extra_price"));
                                            //price[count].setTextColor(0xff000080);
                                            //price[count].setLayoutParams(params);

                                            count++;


                                            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                                                @Override
                                                public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                    RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                                                    String text = radioButton.getText().toString();
                                                    String[] separated = text.split("CAD");
                                                    String name = separated[0];
                                                    String price = separated[1];
                                                    int choId = 0;

                                                    BigDecimal amt = new BigDecimal(price);
                                                    String newname =  name.trim();
                                                    for(int i= 0 ; i < choices1.size(); i++){
                                                        if (choices1.get(i).getCname().equals(newname)){
                                                            Log.d("Detail Avtivity","for loop working for id");
                                                            choId = choices1.get(i).getIde();
                                                            break;
                                                        }
                                                    }
                                                    rdValue = new ExtraProuct(choId,name,Double.valueOf(price));

                                                    String var1 = (String) title.getText();



                                                    if (!radioButtonList.containsKey(var1)){
                                                        Log.d("Radio Button" ,"this radio button has been chosen before");
                                                        radioButtonList.put(var1,rdValue);

                                                    }else {
                                                        radioButtonList.put(var1,rdValue);

                                                    }
//                                                radioButtonList.put(rg.getId(),rdValue);
//                                                Log.d("RadioButton List " , String.valueOf(radioButtonList));
//                                                if (rg.getId()== checkedId){
//                                                    Toast.makeText(DetailActivity.this, "this is equal"   , Toast.LENGTH_SHORT).show();
//                                                }
//
                                                    // BigDecimal invDue = amt.add(amt);
                                                    Toast.makeText(DetailActivity.this, name +  price  , Toast.LENGTH_SHORT).show();
                                                    Log.d("From radio button", "This item was clicked : " + name + " " + price + "$");
                                                }
                                            });
                                        }

                                        ll.addView(rg);
                                    } else{
                                        while(extrasiterator.hasNext()) {
                                            String ek = extrasiterator.next();
                                            JSONObject extra = extras.getJSONObject(ek);
                                            choice2 = new Choice();
                                            final CheckBox checkBox = new CheckBox(DetailActivity.this);
                                            //final TextView price1 = new TextView(DetailActivity.this);


                                            int ChoId = choice2.setIde(extra.getInt("extra_id"));
                                            String Choname = choice2.setCname(extra.getString("extra_name"));
                                            BigDecimal price2 = new BigDecimal(extra.getString("extra_price"));
                                            choices2.add(new Choice(ChoId,Choname));
                                            BigDecimal Proprice = choice2.setCprice(price2);
                                            String Choprice = String.valueOf(Proprice);
                                            checkBox.setText(Choname+ "    CAD" +Choprice);
                                            //checkBox.setText(extra.getString("extra_name"));
                                            //checkBox.setText(Choname);
                                            // BigDecimal price2 = new BigDecimal(extra.getString("extra_price"));
                                            //BigDecimal Proprice = choice1.setCprice(price2);
                                            //price1.setText("C$" + String.valueOf(choice1.setCprice(price2)));
                                            //price1.setText("CAD" + extra.getString("extra_price"));
                                            params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                                            params.gravity = Gravity.CENTER_HORIZONTAL;
                                            checkBox.setOnClickListener(new View.OnClickListener() {

                                                //TODO: finish Implement the extra cart for the app. here !!!
                                                @Override
                                                public void onClick(View v) {
                                                    //is chkIos checked?

                                                    if (((CheckBox) v).isChecked()) {

                                                        String select = ((CheckBox) v).getText().toString();
                                                        String[] separated = select.split("CAD");
                                                        String name = separated[0];
                                                        String price = separated[1];
                                                        BigDecimal amt = new BigDecimal(price);
                                                        //BigDecimal invDue = amt.add(amt);
                                                        Toast.makeText(DetailActivity.this, name  +  price  , Toast.LENGTH_SHORT).show();
                                                        int id = 0;
                                                        String newName = name.trim();
                                                        for (Choice choice:choices2) {
                                                            if(choice.getCname().equals(newName)){
                                                                id = choice.getIde();
                                                                break;
                                                            }
                                                        }
                                                        nameExtra2.add(new ExtraProuct(id,name,Double.valueOf(price)));
                                                        extraTotalPrice += Double.valueOf(price);


                                                        Log.d("Name Extra Array : ", String.valueOf(nameExtra2));
                                                        Log.d("Extra hashmap : " , String.valueOf(extraCart));


                                                    }else {
                                                        String select = ((CheckBox) v).getText().toString();
                                                        String[] separated = select.split("CAD");
                                                        String name = separated[0];
                                                        for (int i=0; i<= nameExtra2.size(); i++) {
                                                            if (nameExtra2.get(i).getExtraName().equals(name) ) {
                                                                nameExtra2.remove(i);
                                                                break;
                                                            }
                                                        }
                                                    }

                                                }
                                            });

                                            ll.addView(checkBox);

                                            //ll.addView(price1);
                                            // price1.setTextColor(0xff000080);
                                            //price1.setLayoutParams(params);
                                        }
                                        listDataHeader.add(pname);

//                                    int listDataHeaderIndex =listDataHeader.indexOf(pname);
//                                    Log.d("ListDataHeader Index = ", String.valueOf(listDataHeaderIndex));
//                                    extraCart.put(listDataHeader.get(listDataHeaderIndex), nameExtra2);



                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("TAG", "There is no Extras for this product" + e.toString());
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        pDialog.dismiss();
                    }
                }

        );
        // Adding request to request queue
        hidePDialog();
        requestQueue.add(obreq);

        AppController.getInstance().addToRequestQueue(extraReq, "jreq");



        //On ordering of product
        onOrderProduct();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
    private void initializeQuantity() {
        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, QUANTITY_LIST);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spQuantity.setAdapter(dataAdapter);
    }
    private void onOrderProduct() {


        btn1 = (Button) findViewById(R.id.btnDone);
        btn1.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {



                ArrayList<ExtraProuct> radioSelectionArrayList = new ArrayList<>(radioButtonList.values());


                if (radioButtonGroup == 1) {
                    if (radioButtonList.isEmpty()){
                        displayMessage("Please choose the Required Selection");
                    }else if (!radioButtonList.isEmpty()){
                        Cart cart = CartHelper.getCart();

                        product.set_ItemId();
                        cart.setItemid(product.get_ItemId());
                        cart.addHeader(product);
                        cart.addHeaderid(product);




                        quantityOrder = (int) spQuantity.getSelectedItem();
                        cart.setQuantity(quantityOrder);
                        if (nameExtra2.isEmpty()){
                            nameExtra2.add(0,new ExtraProuct("No Extra", 0.0));
                            for (int i = 0; i < radioButtonList.size(); i++) {
                                nameExtra2.add(radioSelectionArrayList.get(i));
                            }
                        }else {
                            for (int i = 0; i < radioButtonList.size(); i++) {
                                nameExtra2.add(radioSelectionArrayList.get(i));
                            }
                        }
                        cart.additemWithId(product, nameExtra2,quantityOrder);




                        Intent intent = new Intent(DetailActivity.this, ShoppingCartActivity.class);
                        intent.putExtra("totalExtraPrice", extraTotalPrice);
                        startActivity(intent);
                    }
                }else {
                    Cart cart = CartHelper.getCart();

                    product.set_ItemId();
                    cart.setItemid(product.get_ItemId());
                    cart.addHeader(product);
                    cart.addHeaderid(product);



                    quantityOrder = (int) spQuantity.getSelectedItem();

                    if (nameExtra2.isEmpty()){
                        nameExtra2.add(0,new ExtraProuct("No Extra", 0.0));
                        for (int i = 0; i < radioButtonList.size(); i++) {
                            nameExtra2.add(radioSelectionArrayList.get(i));
                        }
                    }else {
                        for (int i = 0; i < radioButtonList.size(); i++) {
                            nameExtra2.add(radioSelectionArrayList.get(i));

                        }
                    }
                    cart.additemWithId(product, nameExtra2,quantityOrder);




                    Intent intent = new Intent(DetailActivity.this, ShoppingCartActivity.class);
                    intent.putExtra("totalExtraPrice", extraTotalPrice);
                    startActivity(intent);

                }



            }
        });
    }

    private void displayMessage(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setNegativeButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onSlideMenuItemClick(int itemId) {

        switch (itemId) {
            case R.id.item_one:
                Intent p = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(p);
                break;
            case R.id.item_two:
                Intent j = new Intent(getApplicationContext(), CategoriesActivity.class);
                startActivity(j);
                break;
//            case R.id.item_three:
//                Intent k = new Intent(getApplicationContext(), MainActivity.class);
//                startActivity(k);
//                break;
            case MYITEMID:
                Toast.makeText(this, "Dynamically added item selected", Toast.LENGTH_SHORT).show();
                break;
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Profile profile = ProfileHelper.getProfilehelper();
        switch (item.getItemId()) {
            case android.R.id.home: // this is the app icon of the actionbar
                slidemenu.show();
                break;
            case R.id.action_user:
                Intent intent = new Intent(DetailActivity.this, UserProfile.class);
                profile.setFromActivity("detail");
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}





