package com.example.lenovom71e.menuapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 3/4/2018.
 */
public class DataSSL {

    @SerializedName("ssl_card_number")
    @Expose
    private String sslCardNumber;
    @SerializedName("ssl_exp_date")
    @Expose
    private String sslExpDate;
    @SerializedName("ssl_amount")
    @Expose
    private String sslAmount;
    @SerializedName("ssl_invoice_number")
    @Expose
    private String sslInvoiceNumber;
    @SerializedName("ssl_description")
    @Expose
    private String sslDescription;
    @SerializedName("ssl_departure_date")
    @Expose
    private String sslDepartureDate;
    @SerializedName("ssl_completion_date")
    @Expose
    private String sslCompletionDate;
    @SerializedName("ssl_result")
    @Expose
    private String sslResult;
    @SerializedName("ssl_result_message")
    @Expose
    private String sslResultMessage;
    @SerializedName("ssl_txn_id")
    @Expose
    private String sslTxnId;
    @SerializedName("ssl_approval_code")
    @Expose
    private String sslApprovalCode;
    @SerializedName("ssl_cvv2_response")
    @Expose
    private String sslCvv2Response;
    @SerializedName("ssl_avs_response")
    @Expose
    private String sslAvsResponse;
    @SerializedName("ssl_account_balance")
    @Expose
    private String sslAccountBalance;
    @SerializedName("ssl_txn_time")
    @Expose
    private String sslTxnTime;
    @SerializedName("ssl_card_type")
    @Expose
    private String sslCardType;

    /**
     * No args constructor for use in serialization
     *
     */
    public DataSSL() {
    }

    /**
     *
     * @param sslResultMessage
     * @param sslInvoiceNumber
     * @param sslCardType
     * @param sslTxnTime
     * @param sslApprovalCode
     * @param sslCvv2Response
     * @param sslDepartureDate
     * @param sslTxnId
     * @param sslAccountBalance
     * @param sslAmount
     * @param sslResult
     * @param sslAvsResponse
     * @param sslCardNumber
     * @param sslCompletionDate
     * @param sslExpDate
     * @param sslDescription
     */
    public DataSSL(String sslCardNumber, String sslExpDate, String sslAmount, String sslInvoiceNumber, String sslDescription, String sslDepartureDate, String sslCompletionDate, String sslResult, String sslResultMessage, String sslTxnId, String sslApprovalCode, String sslCvv2Response, String sslAvsResponse, String sslAccountBalance, String sslTxnTime, String sslCardType) {
        super();
        this.sslCardNumber = sslCardNumber;
        this.sslExpDate = sslExpDate;
        this.sslAmount = sslAmount;
        this.sslInvoiceNumber = sslInvoiceNumber;
        this.sslDescription = sslDescription;
        this.sslDepartureDate = sslDepartureDate;
        this.sslCompletionDate = sslCompletionDate;
        this.sslResult = sslResult;
        this.sslResultMessage = sslResultMessage;
        this.sslTxnId = sslTxnId;
        this.sslApprovalCode = sslApprovalCode;
        this.sslCvv2Response = sslCvv2Response;
        this.sslAvsResponse = sslAvsResponse;
        this.sslAccountBalance = sslAccountBalance;
        this.sslTxnTime = sslTxnTime;
        this.sslCardType = sslCardType;
    }

    public String getSslCardNumber() {
        return sslCardNumber;
    }

    public void setSslCardNumber(String sslCardNumber) {
        this.sslCardNumber = sslCardNumber;
    }

    public String getSslExpDate() {
        return sslExpDate;
    }

    public void setSslExpDate(String sslExpDate) {
        this.sslExpDate = sslExpDate;
    }

    public String getSslAmount() {
        return sslAmount;
    }

    public void setSslAmount(String sslAmount) {
        this.sslAmount = sslAmount;
    }

    public String getSslInvoiceNumber() {
        return sslInvoiceNumber;
    }

    public void setSslInvoiceNumber(String sslInvoiceNumber) {
        this.sslInvoiceNumber = sslInvoiceNumber;
    }

    public String getSslDescription() {
        return sslDescription;
    }

    public void setSslDescription(String sslDescription) {
        this.sslDescription = sslDescription;
    }

    public String getSslDepartureDate() {
        return sslDepartureDate;
    }

    public void setSslDepartureDate(String sslDepartureDate) {
        this.sslDepartureDate = sslDepartureDate;
    }

    public String getSslCompletionDate() {
        return sslCompletionDate;
    }

    public void setSslCompletionDate(String sslCompletionDate) {
        this.sslCompletionDate = sslCompletionDate;
    }

    public String getSslResult() {
        return sslResult;
    }

    public void setSslResult(String sslResult) {
        this.sslResult = sslResult;
    }

    public String getSslResultMessage() {
        return sslResultMessage;
    }

    public void setSslResultMessage(String sslResultMessage) {
        this.sslResultMessage = sslResultMessage;
    }

    public String getSslTxnId() {
        return sslTxnId;
    }

    public void setSslTxnId(String sslTxnId) {
        this.sslTxnId = sslTxnId;
    }

    public String getSslApprovalCode() {
        return sslApprovalCode;
    }

    public void setSslApprovalCode(String sslApprovalCode) {
        this.sslApprovalCode = sslApprovalCode;
    }

    public String getSslCvv2Response() {
        return sslCvv2Response;
    }

    public void setSslCvv2Response(String sslCvv2Response) {
        this.sslCvv2Response = sslCvv2Response;
    }

    public String getSslAvsResponse() {
        return sslAvsResponse;
    }

    public void setSslAvsResponse(String sslAvsResponse) {
        this.sslAvsResponse = sslAvsResponse;
    }

    public String getSslAccountBalance() {
        return sslAccountBalance;
    }

    public void setSslAccountBalance(String sslAccountBalance) {
        this.sslAccountBalance = sslAccountBalance;
    }

    public String getSslTxnTime() {
        return sslTxnTime;
    }

    public void setSslTxnTime(String sslTxnTime) {
        this.sslTxnTime = sslTxnTime;
    }

    public String getSslCardType() {
        return sslCardType;
    }

    public void setSslCardType(String sslCardType) {
        this.sslCardType = sslCardType;
    }

}