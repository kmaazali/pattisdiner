package com.example.lenovom71e.menuapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.coboltforge.slidemenu.SlideMenuInterface;
import com.example.lenovom71e.menuapp.adapter.SaveSharedPreference;
import com.example.lenovom71e.menuapp.model.CheckOut;
import com.example.lenovom71e.menuapp.model.CheckoutHelper;
import com.example.lenovom71e.menuapp.model.Profile;
import com.example.lenovom71e.menuapp.model.ProfileHelper;

/**
 * Created by admin on 3/5/2018.
 */

public class UserProfile extends AppCompatActivity implements SlideMenuInterface.OnSlideMenuItemClickListener{


    String firstName = "", LastName = "",  phoneNumber = "",postcode ="", userId ="", fromActivity = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        TextView accountNumber = (TextView) findViewById(R.id.accountNumberTxt);
        TextView fullName = (TextView) findViewById(R.id.fullnameEditTxt);
        TextView email = (TextView) findViewById(R.id.emailTxt);
        TextView address = (TextView) findViewById(R.id.addressTxt);
        TextView cityProvince  = (TextView) findViewById(R.id.cityTxt);
        TextView postCode = (TextView) findViewById(R.id.postCodeTxt);
        Button signOut = (Button) findViewById(R.id.signOutBtn);
        Button btnContinue = (Button) findViewById(R.id.continueBtn);
        CheckOut checkout = CheckoutHelper.getCheckout();
        Profile profile = ProfileHelper.getProfilehelper();
        userId = profile.getUserId();

        fromActivity = profile.getFromActivity();

        if (SaveSharedPreference.getAccountAsGuest(getApplicationContext()) == true){
            firstName = SaveSharedPreference.getGuestfname(getApplicationContext());
            LastName = SaveSharedPreference.getGuestlname(getApplicationContext());
            fullName.setText(firstName + " " + LastName);
            accountNumber.setText("Guest");
            email.setText("N/A");
            address.setText(SaveSharedPreference.getGuestAddress(getApplicationContext()));
            String city = SaveSharedPreference.getGuestcity(getApplicationContext());
            String province =  SaveSharedPreference.getGuestProvince(getApplicationContext());
            cityProvince.setText(city + ", " + province);

            phoneNumber = SaveSharedPreference.getGuestphone(getApplicationContext());


            postcode = SaveSharedPreference.getGuestPostcode(getApplicationContext());
            postCode.setText(postcode);
        }else if (userId != "") {
            accountNumber.setText(checkout.getUserId());
            firstName = profile.getFirstName();
            LastName = profile.getLastName();
            fullName.setText(firstName + " " + LastName);
            email.setText(profile.getLoginEmail());
            phoneNumber = profile.getPhone();
            postCode.setText(profile.getPostCode());
            userId = profile.getUserId();
            accountNumber.setText(userId);
            address.setText(profile.getAddress());
            String city = profile.getCiry();
            String province = profile.getProvince();
            cityProvince.setText(city +", "+province);
            postCode.setText(profile.getPostCode());

        }else {

            Intent intent = new Intent(UserProfile.this, LoginActivity.class);
            intent.putExtra("isUserProfile" , true);
            startActivity(intent);
        }

        //TODO: sign out from user
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfile.this, LoginActivity.class);
                intent.putExtra("isUserProfile" , true);
                SaveSharedPreference.clearUserName(getApplicationContext());
                startActivity(intent);
            }
        });

        //TODO: continue function
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (fromActivity == "categories") {
                    Intent intent = new Intent(UserProfile.this, CategoriesActivity.class);
                    startActivity(intent);
//                } else if (fromActivity == "detail") {
//                    Intent intent = new Intent(UserProfile.this, DetailActivity.class);
//                    startActivity(intent);
//                } else if (fromActivity == "product") {
//                    Intent intent = new Intent(UserProfile.this, ProductActivity.class);
//                    startActivity(intent);
//                }
//                else {

//                    finish();
//                }
            }
        });


    }



    @Override
    public void onSlideMenuItemClick(int itemId) {

    }

    @Override
    public void onBackPressed(){

    }
}
