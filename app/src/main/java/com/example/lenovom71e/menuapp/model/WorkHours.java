package com.example.lenovom71e.menuapp.model;

/**
 * Created by admin on 2017-07-28.
 */

public class WorkHours {

    String open,close,status;

    public WorkHours(){
    }
    public WorkHours(String open, String close, String status){
        this.open = open;
        this.close = close;
        this.status =  status;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
