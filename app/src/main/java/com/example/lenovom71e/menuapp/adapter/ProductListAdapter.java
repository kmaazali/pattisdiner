package com.example.lenovom71e.menuapp.adapter;

/**
 * Created by STUDENT on 2/24/2016.
 */
import com.example.lenovom71e.menuapp.R;
import com.example.lenovom71e.menuapp.app.AppController;
import com.example.lenovom71e.menuapp.model.Product;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class ProductListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Product> productItems;
   // private int[] colors = new int[] { 0x302C6700, 0x30FFCC00 };
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ProductListAdapter(Activity activity, List<Product> productItems) {
        this.activity = activity;
        this.productItems = productItems;
    }

    @Override
    public int getCount() {
        return productItems.size();
    }

    @Override
    public Object getItem(int location) {
        return productItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_product, parent, false);


        TextView pname = (TextView) convertView.findViewById(R.id.pname);
        TextView pdescription = (TextView) convertView.findViewById(R.id.pdescription);
        TextView price = (TextView) convertView.findViewById(R.id.price);
        TextView id = (TextView) convertView.findViewById(R.id.product_id);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);

        // getting movie data for the row
        Product m = productItems.get(position);
        id.setText(String.valueOf(m.getPid()));
        // title
        pname.setText(m.getPname());

        // rating
        pdescription.setText(m.getPdescription());

        // release year
        price.setText("Price: C$" + m.getPrice());
        // thumbnail image
        thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);

        //int colorPos = position % colors.length;
        //convertView.setBackgroundColor(colors[colorPos]);
        return convertView;

    }

}
