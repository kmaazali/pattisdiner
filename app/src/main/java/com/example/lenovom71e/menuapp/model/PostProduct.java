package com.example.lenovom71e.menuapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/10/2018.
 */

public class PostProduct {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("oid")
    @Expose
    private Integer oid;
    @SerializedName("amount")
    @Expose
    private Double amount;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString(){
        return "Post Product {" + "message = " + message + " , Oid  = " + oid + " , amount = " + amount + "}";
    }
}
