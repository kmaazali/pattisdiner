package com.android.tonyvu.sc.model;

/**
 * Created by admin on 2017-06-28.
 */

public class ExtraProuct {

    int extraId;
    String extraName;
    double extraPrice;
    double totalExtraPrice;

    int quantity;

    /**
     *
     * @param name Name of the Extra item
     * @param price Price of the Extra Item
     */

    public ExtraProuct(String name, double price){
        extraName = name;
        extraPrice = price;

    }
    public ExtraProuct(int id, String name, double price){
        extraId = id;
        extraName = name;
        extraPrice = price;

    }

    public int getExtraId() {
        return extraId;
    }

    public void setExtraId(int extraId) {
        this.extraId = extraId;
    }

    public String getExtraName() {
        return extraName;
    }


    public double getExtraPrice() {
        return extraPrice;
    }

    public double getTotalExtraPrice() {
        return totalExtraPrice;
    }

    public void setTotalExtraPrice(double totalExtraPrice) {
        this.totalExtraPrice = totalExtraPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
